package view;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Toolkit;
import java.util.ArrayList;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.SwingConstants;
import javax.swing.table.DefaultTableModel;
import controller.ControllerEscenario1;


public class MainWindows extends JFrame{

	private static final long serialVersionUID = 1L;
	private static String[] HEAD = {"Id tiempo", "Tiempo inicial", "Tiempo final", "Delta", "¿Completó?"};
	private DefaultTableModel tableModel;
	private JTable tableProductList;
	private PanelBotones panelMiddleSquares;
	private GridBagConstraints gbc;
	private ArrayList<Object[]> listaAux;
	private JLabel message;
	
	public MainWindows(ControllerEscenario1 controllerEscenario1) {
		Dimension pantalla = Toolkit.getDefaultToolkit().getScreenSize();
		setSize(pantalla.width, pantalla.height);
		setTitle("Pseudorandom Numbers");
		setDefaultCloseOperation(EXIT_ON_CLOSE);
		getContentPane().setBackground(Color.WHITE);
		setLayout(new GridBagLayout());
		gbc = new GridBagConstraints();	

		//PANEL DE BOTONES DE RUTA
		gbc.gridx = 0;
		gbc.gridy = 0;
		gbc.weightx = 1;
		gbc.weighty = 1;
		gbc.gridheight = 1;
		gbc.gridwidth = 1;
		gbc.insets.set(6,6,6,6);
		gbc.fill = GridBagConstraints.BOTH;
		panelMiddleSquares =  new PanelBotones(controllerEscenario1);
		add(panelMiddleSquares,gbc);
		
		
		//TABLA
		gbc.gridx = 1;
		gbc.gridy = 0;
		gbc.weightx = 1;
		gbc.weighty = 1;
		gbc.gridheight = 1;
		gbc.gridwidth = 1;
		gbc.insets.set(6,6,6,6);
		gbc.fill = GridBagConstraints.BOTH;
		tableModel = new DefaultTableModel();
		tableModel.setColumnIdentifiers(HEAD);
		tableProductList = new JTable(tableModel);
		tableProductList.setRowHeight(40);
		TableRender model = new TableRender();
		CellEditor cellEditor = new CellEditor(null);
		model.setHorizontalAlignment(SwingConstants.CENTER);
		tableProductList.setDefaultRenderer(Object.class, model);
		tableProductList.setDefaultEditor(Object.class, cellEditor);
		tableProductList.getTableHeader().setBackground(Color.WHITE);
		tableProductList.setGridColor(Color.GRAY);
		JScrollPane scroll = new JScrollPane(tableProductList);
		scroll.getVerticalScrollBar().setBackground(Color.WHITE);
		scroll.getViewport().setBackground(Color.WHITE);
		scroll.setBackground(Color.WHITE);
		scroll.setOpaque(true);
		add(scroll,gbc);
		
		//PANEL PROMEDIO
		gbc.gridx = 0;
		gbc.gridy = 1;
		gbc.weightx = 1;
		gbc.weighty = 1;
		gbc.gridheight = 1;
		gbc.gridwidth = 2;
		gbc.insets.set(6,6,6,6);
		gbc.fill = GridBagConstraints.BOTH;
		message =  new JLabel("PROMEDIO TIEMPO: ");
		message.setHorizontalAlignment(SwingConstants.CENTER);
		add(message,gbc);
		setVisible(true);
	}
	
	public void addDataToTableRuta(ArrayList<Object[]> results, Double promedio) {
		listaAux =  results;
		removePage();
		tableModel.setColumnIdentifiers(HEAD);
		repaint();
		for (int i = 0; i < results.size(); i++) {
				tableModel.addRow(results.get(i));
		}
		message.setText("PROMEDIO TIEMPO: " + promedio + " segundos");
	}
			
	public void removePage() {
		for (int i = tableModel.getRowCount() - 1; i >= 0; i--) {
			tableModel.removeRow(i);
		}
	}
	
	public void addDataToTableRutaTotal(ArrayList<Object[]> results, int prom) {
		listaAux =  results;
		removePage();
		String[] HEAD = {"Id ", "Delta Ruta 1", "Delta Ruta 2", "Delta Ruta 3", "Delta Ruta 4", "Tiempo total", 
				"¿Completó?"};
		tableModel.setColumnIdentifiers(HEAD);
		repaint();		
		for (int i = 0; i < results.size(); i++) {
				tableModel.addRow(results.get(i));
		}
		message.setText("PROMEDIO TIEMPO RUTA COMPLETA: " + prom + " segundos");
	}
	
	public ArrayList<Object[]> getListaAux() {
		return listaAux;
	}

	
	public void addResultsUniformDistribution(ArrayList<Object[]> results) {
		if(results != null) {
			removePage();
			String[] HEAD = {"Ri", "Ni"};
			tableModel.setColumnIdentifiers(HEAD);
			repaint();		
			for (int i = 0; i < results.size(); i++) {
					tableModel.addRow(results.get(i));
			}
			message.setText("La Distribucion  uniforme es correcta");
			message.setBackground(Color.decode("#395EFF"));
			message.setFont(new Font("Arial Black", Font.PLAIN, 12));
			message.setForeground(Color.GREEN); 
			
		}else {
			message.setText("La Distribucion uniforme no correcta");
			message.setBackground(Color.decode("#FF435D"));
			message.setFont(new Font("Arial Black", Font.PLAIN, 12));
			message.setForeground(Color.RED); 
		}
		
		
	}
	
	
	public static void main(String[] args) {
		new MainWindows(null);
		
	}	

}
