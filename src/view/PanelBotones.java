package view;

import java.awt.Color;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.time.temporal.JulianFields;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.SwingConstants;

import controller.Action;
import controller.ControllerEscenario1;

public class PanelBotones extends JPanel {

	private static final long serialVersionUID = 1L;
	private JTextField seed;
	private JTextField quantity;
	private JTextField txtId;

	public PanelBotones(ControllerEscenario1 controller) {
		// TODO Auto-generated constructor stub
		setLayout(new GridBagLayout());
		GridBagConstraints gbc = new GridBagConstraints();	
		
		gbc.gridx = 0;
		gbc.gridy = 0;
		gbc.weightx = 1;
		gbc.weighty = 1;
		gbc.gridheight = 1;
		gbc.gridwidth = 1;
		gbc.insets.set(6,6,6,6);
		gbc.fill = GridBagConstraints.BOTH;
		JButton btnRuta1 = new JButton("RUTA 1");
		btnRuta1.addActionListener(controller);
		btnRuta1.setActionCommand(Action.RUTA_1.name());
		btnRuta1.setBackground(Color.decode("#BD79FF"));
		btnRuta1.setFont(new Font("Arial Black", Font.PLAIN, 12));
		btnRuta1.setForeground(Color.WHITE); 
		add(btnRuta1, gbc);

		gbc.gridx = 0;
		gbc.gridy = 1;
		gbc.weightx = 1;
		gbc.weighty = 1;
		gbc.gridheight = 1;
		gbc.gridwidth = 1;
		gbc.insets.set(6,6,6,6);
		gbc.fill = GridBagConstraints.BOTH;
		JButton btnRuta2 = new JButton("RUTA 2");
		btnRuta2.addActionListener(controller);
		btnRuta2.setActionCommand(Action.RUTA_2.name());
		btnRuta2.setBackground(Color.decode("#BD79FF"));
		btnRuta2.setFont(new Font("Arial Black", Font.PLAIN, 12));
		btnRuta2.setForeground(Color.WHITE); 
		add(btnRuta2, gbc);
		
		gbc.gridx = 0;
		gbc.gridy = 2;
		gbc.weightx = 1;
		gbc.weighty = 1;
		gbc.gridheight = 1;
		gbc.gridwidth = 1;
		gbc.insets.set(6,6,6,6);
		gbc.fill = GridBagConstraints.BOTH;
		JButton btnRuta3 = new JButton("RUTA 3");
		btnRuta3.addActionListener(controller);
		btnRuta3.setActionCommand(Action.RUTA_3.name());
		btnRuta3.setBackground(Color.decode("#BD79FF"));
		btnRuta3.setFont(new Font("Arial Black", Font.PLAIN, 12));
		btnRuta3.setForeground(Color.WHITE); 
		add(btnRuta3, gbc);
		
		gbc.gridx = 0;
		gbc.gridy = 3;
		gbc.weightx = 1;
		gbc.weighty = 1;
		gbc.gridheight = 1;
		gbc.gridwidth = 1;
		gbc.insets.set(6,6,6,6);
		gbc.fill = GridBagConstraints.BOTH;
		JButton btnRuta4 = new JButton("RUTA 4");
		btnRuta4.addActionListener(controller);
		btnRuta4.setActionCommand(Action.RUTA_4.name());
		btnRuta4.setBackground(Color.decode("#BD79FF"));
		btnRuta4.setFont(new Font("Arial Black", Font.PLAIN, 12));
		btnRuta4.setForeground(Color.WHITE); 
		add(btnRuta4, gbc);
		
		gbc.gridx = 0;
		gbc.gridy = 4;
		gbc.weightx = 1;
		gbc.weighty = 1;
		gbc.gridheight = 1;
		gbc.gridwidth = 1;
		gbc.insets.set(6,6,6,6);
		gbc.fill = GridBagConstraints.BOTH;
		JButton btnRutaTotal = new JButton("RUTA TOTAL");
		btnRutaTotal.addActionListener(controller);
		btnRutaTotal.setActionCommand(Action.RUTA_TOTAL.name());
		btnRutaTotal.setBackground(Color.decode("#BD79FF"));
		btnRutaTotal.setFont(new Font("Arial Black", Font.PLAIN, 12));
		btnRutaTotal.setForeground(Color.WHITE); 
		add(btnRutaTotal, gbc);
		
	}

}
