/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package view;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.EventQueue;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Image;
import java.net.URL;
import java.util.ArrayList;
import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTabbedPane;
import javax.swing.JTable;
import javax.swing.UIManager;
import javax.swing.border.TitledBorder;
import javax.swing.table.DefaultTableModel;
import logic.scenaryThree.InterfaceScenaryThree;
import logic.scenaryThree.ScenaryThree;
import utils.Person;
import utils.RandomUtils;

/**
 *
 * @author wilber romero
 */
public class EscenarioTresGUI extends JFrame implements InterfaceScenaryThree {

    private JLabel promedioRutaUno;
    private JLabel promedioRutaDos;
    private JLabel promedioRutaTres;
    private JLabel promedioRutaCuatro;
    private JLabel cantidadlunes;

    private DefaultTableModel dtm1;
    private JTable table1;
    private static final int N = 16;

    private DefaultTableModel dtm2;
    private JTable table2;

    private DefaultTableModel dtm3;
    private JTable table3;

    private DefaultTableModel dtm4;
    private JTable table4;

    private DefaultTableModel dtm5;
    private JTable table5;
    private JPanel j;
    public URL fondo;
    public Image imagenfondo;
    InterfaceScenaryThree scenaryThree;

    public EscenarioTresGUI(InterfaceScenaryThree interfaceScenaryThree) {
        try {
            UIManager.setLookAndFeel("com.sun.java.swing.plaf.nimbus.NimbusLookAndFeel");
        } catch (Exception e) {
            e.printStackTrace();
        }
        this.scenaryThree = interfaceScenaryThree;
        display();

    }

    private void display() {
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        this.setSize(1400, 1400);
        setLocationRelativeTo(null);

        promedioRutaUno = new JLabel();
        Font auxFont = promedioRutaUno.getFont();
        promedioRutaUno.setFont(new Font(auxFont.getFontName(), auxFont.getStyle(), 30));
        promedioRutaUno.setBackground(Color.GREEN);
        promedioRutaUno.setOpaque(true);

        promedioRutaDos = new JLabel();
        Font auxFont2 = promedioRutaDos.getFont();
        promedioRutaDos.setFont(new Font(auxFont2.getFontName(), auxFont2.getStyle(), 30));
        promedioRutaDos.setBackground(Color.RED);
        promedioRutaDos.setOpaque(true);

        promedioRutaTres = new JLabel();
        Font auxFont3 = promedioRutaTres.getFont();
        promedioRutaTres.setFont(new Font(auxFont3.getFontName(), auxFont3.getStyle(), 30));
        promedioRutaTres.setBackground(Color.BLUE);
        promedioRutaTres.setOpaque(true);

        promedioRutaCuatro = new JLabel();
        Font auxFont4 = promedioRutaCuatro.getFont();
        promedioRutaCuatro.setFont(new Font(auxFont4.getFontName(), auxFont4.getStyle(), 30));
        promedioRutaCuatro.setBackground(Color.ORANGE);
        promedioRutaCuatro.setOpaque(true);

        cantidadlunes = new JLabel();
        Font auxFontl = cantidadlunes.getFont();
        cantidadlunes.setFont(new Font(auxFontl.getFontName(), auxFontl.getStyle(), 30));
        cantidadlunes.setBackground(Color.MAGENTA);
        cantidadlunes.setOpaque(true);

        fondo = this.getClass().getResource("/imagenes/ruta2.jpg");
        imagenfondo = new ImageIcon(fondo).getImage();
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        String[] columnNames = {"id", "Hora Inicio", "Hora Fina", "Duracion", "Ruta completa"};
        Object[][] datos = {}; //vacio 

        dtm1 = new DefaultTableModel(datos, columnNames);
        dtm2 = new DefaultTableModel(datos, columnNames);
        dtm3 = new DefaultTableModel(datos, columnNames);
        dtm4 = new DefaultTableModel(datos, columnNames);
        dtm5 = new DefaultTableModel(datos, columnNames);

        table1 = new JTable(dtm1) {

            @Override
            public Dimension getPreferredScrollableViewportSize() {
                return new Dimension(550, getRowHeight() * N / 2);
            }
        };
        table2 = new JTable(dtm2) {

            @Override
            public Dimension getPreferredScrollableViewportSize() {
                return new Dimension(550, getRowHeight() * N / 2);
            }
        };
        table3 = new JTable(dtm3) {

            @Override
            public Dimension getPreferredScrollableViewportSize() {
                return new Dimension(550, getRowHeight() * N / 2);
            }
        };
        table4 = new JTable(dtm4) {

            @Override
            public Dimension getPreferredScrollableViewportSize() {
                return new Dimension(550, getRowHeight() * N / 2);
            }
        };
        table5 = new JTable(dtm5) {

            @Override
            public Dimension getPreferredScrollableViewportSize() {
                return new Dimension(550, getRowHeight() * N / 2);
            }
        };

        this.setPromedioRutaUno(Double.toString(this.getAverageRoute1()));
        this.setPromedioRutaDos(Double.toString(this.getAverageRoute2()));
        this.setPromedioRutaTres(Double.toString(this.getAverageRoute3()));
        this.setPromedioRutaCuatro(Double.toString(this.getAverageRoute4()));
        this.setCantidadLunes(Integer.toString(this.getAmountOfMondays()));

        for (Person person : getPersonsOfRouteOne()) {
            this.addNewRowRutaUno(Integer.toString(person.getNumberOfPerson()), person.getInitialTime(), person.getFinalTime(), Integer.toString(person.getDuration()), person.isCompleted() ? "Si" : "no");

        }
        for (Person person : getPersonsOfRouteTwo()) {
            this.addNewRowRutaDos(Integer.toString(person.getNumberOfPerson()), person.getInitialTime(), person.getFinalTime(), Integer.toString(person.getDuration()), person.isCompleted() ? "Si" : "no");

        }

        for (Person person : getPersonsOfRouteThree()) {
            this.addNewRowRutaTres(Integer.toString(person.getNumberOfPerson()), person.getInitialTime(), person.getFinalTime(), Integer.toString(person.getDuration()), person.isCompleted() ? "Si" : "no");

        }
        for (Person person : getPersonsOfRouteFour()) {
            this.addNewRowRutaCuatro(Integer.toString(person.getNumberOfPerson()), person.getInitialTime(), person.getFinalTime(), Integer.toString(person.getDuration()), person.isCompleted() ? "Si" : "no");

        }
        for (Person person : getAllPersonsScenaryThree()) {
            this.addNewRowAll(Integer.toString(person.getNumberOfPerson()), person.getInitialTime(), person.getFinalTime(), Integer.toString(person.getDuration()), person.isCompleted() ? "Si" : "no");

        }

        j = new JPanel() {

            public void paintComponent(Graphics g) {
                g.drawImage(imagenfondo, 0, 0, getWidth(), getHeight(), this);

            }
        };

        JScrollPane jsp = new JScrollPane();
        jsp.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Ruta 1", TitledBorder.TOP, TitledBorder.BOTTOM, new Font("times new roman", Font.PLAIN, 20), Color.RED));
        jsp.setViewportView(table1);
        j.add(jsp);

        JScrollPane jsp3 = new JScrollPane();
        jsp3.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Ruta 2", TitledBorder.TOP, TitledBorder.BOTTOM, new Font("times new roman", Font.PLAIN, 20), Color.RED));
        jsp3.setViewportView(table2);
        j.add(jsp3);

        JScrollPane jsp4 = new JScrollPane();
        jsp4.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Ruta 3", TitledBorder.TOP, TitledBorder.BOTTOM, new Font("times new roman", Font.PLAIN, 20), Color.RED));
        jsp4.setViewportView(table3);
        j.add(jsp4);

        JScrollPane jsp5 = new JScrollPane();
        jsp5.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Ruta 4", TitledBorder.TOP, TitledBorder.BOTTOM, new Font("times new roman", Font.PLAIN, 20), Color.RED));
        jsp5.setViewportView(table4);
        j.add(jsp5);

        JScrollPane jsp6 = new JScrollPane();
        jsp6.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Ruta 5", TitledBorder.TOP, TitledBorder.BOTTOM, new Font("Agency FB", Font.BOLD, 20), Color.red));
        jsp6.setViewportView(table5);

        j.add(jsp6);

        JTabbedPane jtp = new JTabbedPane();

        jtp.add("Escenario tres", j);

        add(jtp);
        iniciarJlabels();
        setVisible(true);

    }

    public void addNewRowRutaUno(String id, String horainicio, String horafinal, String duracion, String rutaCompleta) {
        Object[] newRow = {id, horainicio, horafinal, duracion, rutaCompleta};
        dtm1.addRow(newRow);

    }

    public void addNewRowRutaDos(String id, String horainicio, String horafinal, String duracion, String rutaCompleta) {
        Object[] newRow2 = {id, horainicio, horafinal, duracion, rutaCompleta};
        dtm2.addRow(newRow2);

    }

    public void addNewRowRutaTres(String id, String horainicio, String horafinal, String duracion, String rutaCompleta) {
        Object[] newRow3 = {id, horainicio, horafinal, duracion, rutaCompleta};
        dtm3.addRow(newRow3);

    }

    public void addNewRowRutaCuatro(String id, String horainicio, String horafinal, String duracion, String rutaCompleta) {
        Object[] newRow4 = {id, horainicio, horafinal, duracion, rutaCompleta};
        dtm4.addRow(newRow4);

    }

    public void addNewRowAll(String id, String horainicio, String horafinal, String duracion, String rutaCompleta) {
        Object[] newRow5 = {id, horainicio, horafinal, duracion, rutaCompleta};
        dtm5.addRow(newRow5);

    }

    public void iniciarJlabels() {

        j.add(promedioRutaUno);
        j.add(promedioRutaDos);
        j.add(promedioRutaTres);
        j.add(promedioRutaCuatro);
        j.add(cantidadlunes);

    }

    public void setPromedioRutaUno(String ruta) {
        this.promedioRutaUno.setText("Prom. ruta 1: " + ruta);
        this.repaint();
    }

    public void setPromedioRutaDos(String nocompleto) {
        this.promedioRutaDos.setText("    Prom. ruta 2: " + nocompleto);
        this.repaint();
    }

    public void setPromedioRutaTres(String promedio) {
        this.promedioRutaTres.setText("   Prom. ruta 3: " + promedio);
        this.repaint();

    }

    public void setPromedioRutaCuatro(String promedio) {
        this.promedioRutaCuatro.setText("   Prom. ruta 4: " + promedio);
        this.repaint();

    }

    public void setCantidadLunes(String promedio) {
        this.cantidadlunes.setText(" Cantidad Lunes: " + promedio);
        this.repaint();

    }

    @Override
    public double getAverageRoute1() {
        return scenaryThree.getAverageRoute1();
    }

    @Override
    public double getAverageRoute2() {
        return scenaryThree.getAverageRoute2();
    }

    @Override
    public double getAverageRoute3() {
        return scenaryThree.getAverageRoute3();
    }

    @Override
    public double getAverageRoute4() {
        return scenaryThree.getAverageRoute4();
    }

    @Override
    public ArrayList<Person> getPersonsOfRouteOne() {
        return scenaryThree.getPersonsOfRouteOne();
    }

    @Override
    public ArrayList<Person> getPersonsOfRouteTwo() {
        return scenaryThree.getPersonsOfRouteTwo();
    }

    @Override
    public ArrayList<Person> getPersonsOfRouteThree() {
        return scenaryThree.getPersonsOfRouteThree();
    }

    @Override
    public ArrayList<Person> getPersonsOfRouteFour() {
        return scenaryThree.getPersonsOfRouteFour();
    }

    @Override
    public ArrayList<Person> getAllPersonsScenaryThree() {
        return scenaryThree.getAllPersonsScenaryThree();
    }

    @Override
    public int getAmountOfMondays() {
        return scenaryThree.getAmountOfMondays();
    }
}
