package view;

import logic.scenaryThree.InterfaceScenaryThree;
import logic.scenaryThree.ScenaryThree;
import logic.scenaryTwo.InterfaceScenaryTwo;
import logic.scenaryTwo.ScenaryTwo;

public class ControllerView {
	private InterfaceScenaryTwo interfaceScenaryTwo;
	private InterfaceScenaryThree interfaceScenaryThree;

	public ControllerView() {
		this.interfaceScenaryTwo = new ScenaryTwo(300 /* Cantidad de datos a Simular en 1 Lunes */);
		this.interfaceScenaryThree = new ScenaryThree(19, 1);
	}
}
