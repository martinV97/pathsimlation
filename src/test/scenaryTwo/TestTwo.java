package test.scenaryTwo;

import logic.scenaryTwo.InterfaceScenaryTwo;
import logic.scenaryTwo.ScenaryTwo;
import utils.PlantillaDato;

public class TestTwo {

	public static void main(String[] args) {
		int cantidadDeDatos = 500; // Cantidad de datos a Simular en 1 Lunes
		InterfaceScenaryTwo scenaryTwo = new ScenaryTwo(cantidadDeDatos);
		PlantillaDato[] datosCompletaronRecorrido = scenaryTwo.getDatosCompletaronRecorrido();
		System.out.println("--- Personas o Grupos que Pasaron Todas las Pruebas ---");
		for (PlantillaDato plantillaDato : datosCompletaronRecorrido) {
			System.out.println(plantillaDato);
		}
		
		System.out.println("Cantidad completaron 1: " + scenaryTwo.getCantidadCompletaronTramo1());
		System.out.println("Cantidad completaron 2: " + scenaryTwo.getCantidadCompletaronTramo2());
		System.out.println("Cantidad completaron 3: " + scenaryTwo.getCantidadCompletaronTramo3());
		System.out.println("Cantidad completaron 4: " + scenaryTwo.getCantidadCompletaronTramo4());
		System.out.println("Cantidad completaron Total: " + scenaryTwo.getCantidadCompletaronTodoElRecorrido());
		
		System.out.println("Cantidad NO completaron 1: " + scenaryTwo.getCantidadNoCompletaronTramo1());
		System.out.println("Cantidad NO completaron 2: " + scenaryTwo.getCantidadNoCompletaronTramo2());
		System.out.println("Cantidad NO completaron 3: " + scenaryTwo.getCantidadNoCompletaronTramo3());
		System.out.println("Cantidad NO completaron 4: " + scenaryTwo.getCantidadNoCompletaronTramo4());
		
		System.out.println("Promedio Tiempo 1: " + scenaryTwo.getPromedioTiempoCaminandoTramo1());
		System.out.println("Promedio Tiempo 2: " + scenaryTwo.getPromedioTiempoCaminandoTramo2());
		System.out.println("Promedio Tiempo 3: " + scenaryTwo.getPromedioTiempoCaminandoTramo3());
		System.out.println("Promedio Tiempo 4: " + scenaryTwo.getPromedioTiempoCaminandoTramo4());
		System.out.println("Promedio Tiempo Todo: " + scenaryTwo.getPromedioTiempoCaminandoTodoElRecorrido());
	}

}
