package test.scenaryThree;

import logic.scenaryThree.ScenaryThree;
import utils.RandomUtils;

/**
 * Clase principal para la ejecución de la simulación del escenario 3.
 * @author Duvis Gómez && Martin Vivanco
 * @version 1.0
 */
public class TestThree {

	public static void main(String[] args) {
		ScenaryThree scenaryThree = new ScenaryThree(RandomUtils.randomBetween(97, 131), 2);
		System.out.println("Promedio ruta 1: " + scenaryThree.getAverageRoute1());
		System.out.println("Promedio ruta 2: " + scenaryThree.getAverageRoute2());
		System.out.println("Promedio ruta 3: " + scenaryThree.getAverageRoute3());
		System.out.println("Promedio ruta 4:  " + scenaryThree.getAverageRoute4());
		scenaryThree.showStatistics();
		System.out.println("Cantidad de lunes necesarios: " + scenaryThree.getAmountOfMondays());
//		Person person = new Person(0, 'M', "11:59");
//		System.out.println(person.getFinalTime(person.getInitialTime(), 2));
//		System.out.println(person.getDuration());
	}

}
