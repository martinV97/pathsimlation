package test.scenaryOne;

import java.lang.reflect.Array;
import java.util.ArrayList;

import logic.scenaryOne.ScenaryOne;
import utils.scenaryOne.EnumRoute;
import utils.scenaryOne.Route;

public class TestOne {

	public static void main(String[] args) {
		
		//Datos  ruta 1
		ArrayList<Route> datosRutaOne =  new ArrayList<Route>();
		datosRutaOne.add(new Route(EnumRoute.ONE, 60, 180));
		datosRutaOne.add(new Route(EnumRoute.ONE, 240, 420));
		datosRutaOne.add(new Route(EnumRoute.ONE, 360, 660));
		datosRutaOne.add(new Route(EnumRoute.ONE, 420, 580));
		datosRutaOne.add(new Route(EnumRoute.ONE, 480, 690));
		datosRutaOne.add(new Route(EnumRoute.ONE, 540, 730));
		datosRutaOne.add(new Route(EnumRoute.ONE, 720, 900));
		datosRutaOne.add(new Route(EnumRoute.ONE, 780, 870));
		datosRutaOne.add(new Route(EnumRoute.ONE, 840, 1030));
		datosRutaOne.add(new Route(EnumRoute.ONE, 840, 1050));
		datosRutaOne.add(new Route(EnumRoute.ONE, 900, 1100));
		datosRutaOne.add(new Route(EnumRoute.ONE, 1080, 1353));
		datosRutaOne.add(new Route(EnumRoute.ONE, 1140, 1440));
		datosRutaOne.add(new Route(EnumRoute.ONE, 1380, 1620));
		datosRutaOne.add(new Route(EnumRoute.ONE, 1500, 1620));
		//Datos ruta 3
		
		ArrayList<Route> datosRutaTwo =  new ArrayList<Route>();
		datosRutaTwo.add(new Route(EnumRoute.TWO, 0, 232));
		datosRutaTwo.add(new Route(EnumRoute.TWO, 1407, 1478));
		
		
		
		
		//Datos ruta 3
		
		ArrayList<Route> datosRutaTheee =  new ArrayList<Route>();
		datosRutaTheee.add(new Route(EnumRoute.THREE, 60, 295));
		datosRutaTheee.add(new Route(EnumRoute.THREE, 1380, 1436));
		
		//Datos ruta 4
		
		
		ArrayList<Route> datosRutaFour =  new ArrayList<Route>();
		datosRutaFour.add(new Route(EnumRoute.FOUR, 250, 400));
		datosRutaFour.add(new Route(EnumRoute.FOUR, 1328, 1350));
		datosRutaFour.add(new Route(EnumRoute.FOUR, 1380, 1442));
		
		// Siulacion
		
		
		ScenaryOne one =  new ScenaryOne();
		one.setRutaUno(datosRutaOne);
		one.setRutaDos(datosRutaTwo);
		one.setRutaTres(datosRutaTheee);
		one.setRutaCuatro(datosRutaFour);
		
		System.out.println("Promedio ruta 1: " + one.promedioTiemposEnSegundos(one.getRutaUno())/60  + " minutos");
		System.out.println("Promedio ruta 2: " + one.promedioTiemposEnSegundos(one.getRutaDos())/60  + " minutos");
		System.out.println("Promedio ruta 3: " + one.promedioTiemposEnSegundos(one.getRutaTres())/60  + " minutos");
		System.out.println("Promedio ruta 4: " + one.promedioTiemposEnSegundos(one.getRutaCuatro())/60  + " minutos");
	
		
		System.out.println("************ Ruta 1**********************");
		one.imprimirInfo(one.getRutaUno());
		
		System.out.println("***************Ruta 2*******************");
		
		one.run(one.getRutaDos());
		one.imprimirInfo(one.getRutaUno());
		
		System.out.println("***************Ruta 3*******************");
		
		one.run(one.getRutaTres());
		one.imprimirInfo(one.getRutaUno());
		
		System.out.println("***************Ruta 4*******************");
		
		one.run(one.getRutaCuatro());
		one.imprimirInfo(one.getRutaUno());
		
		
		System.out.println("*************** rutas que terminaron *******************");
		
		ArrayList<Route> rutasCompletas =  one.getTiemposQueCumplieronLaRutaCompleta();
		for (int i = 0; i < rutasCompletas.size(); i++) {
			System.out.println("El tiempo :" + rutasCompletas.get(i).getId() +  " , termino en un tiempo de :" + rutasCompletas.get(i).getDeltaAcumulado()/60 + " minutos");
		}
		
		
		System.out.println("*************** promedio rutas que terminaron *******************");
		System.out.println((double) (one.promedioTiempoRutaCompleta()/60) + " minutos");
		
		// TODO Auto-generated method stub

	}

}
