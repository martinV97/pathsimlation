package utils;

import java.util.ArrayList;

/**
 * 
 * @author Duvis Gómez && Martin Vivanco
 * @version 1.0
 */
public class Route {
	private String name;
	/**
	 * Tiempo mínimo para realizar la ruta
	 */
	private int timeMin;
	/**
	 * Tiempo máximo para realizar la ruta
	 */
	private int timeMax;
	private ArrayList<Person> listHistoricalPeople;
	
	public Route(String name) {
		listHistoricalPeople = new ArrayList<Person>();
		this.name = name;
	}
	
	public double timeInRoute() {
		return RandomUtils.randomBetween(timeMin, timeMax);
	}

	/**
	 * @return  Método que obtiene el valor de la propiedad name.
	 */
	public String getName() {
		return name;
	}

	/**
	 * @param Método que asigna el valor de la propiedad name.
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * @return  Método que obtiene el valor de la propiedad listHistoricalPeople.
	 */
	public ArrayList<Person> getListHistoricalPeople() {
		return listHistoricalPeople;
	}

	/**
	 * @param Método que asigna el valor de la propiedad listHistoricalPeople.
	 */
	public void setListHistoricalPeople(ArrayList<Person> listHistoricalPeople) {
		this.listHistoricalPeople = listHistoricalPeople;
	}

	/**
	 * @return  Método que obtiene el valor de la propiedad timeMin.
	 */
	public int getTimeMin() {
		return timeMin;
	}

	/**
	 * @param Método que asigna el valor de la propiedad timeMin.
	 */
	public void setTimeMin(int timeMin) {
		this.timeMin = timeMin;
	}

	/**
	 * @return  Método que obtiene el valor de la propiedad timeMax.
	 */
	public int getTimeMax() {
		return timeMax;
	}

	/**
	 * @param Método que asigna el valor de la propiedad timeMax.
	 */
	public void setTimeMax(int timeMax) {
		this.timeMax = timeMax;
	}
	
	
	
}
