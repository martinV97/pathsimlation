package utils;

/**
 * Clase es utilizada para almacenar tanto la informacion de los datos SEMILLA como cada uno de los datos generados
 * a traves de la simulacion
 */
public class PlantillaDato implements Cloneable {
	/**
	 * Representa un contador para mostrar en la tabla e identificar que numero de grupo o individuo es
	 */
	private int numero;
	/**
	 * tiempo que tarda el grupo o individuo en completar la ruta
	 */
	private int tiempoCaminandoSegundos;
	/**
	 * cantidad de hombre que transitan la ruta
	 */
	private int cantidadHombres;
	/**
	 * cantidad de mujeres que transitan la ruta
	 */
	private int cantidadMujeres;
	/**
	 * Especifica si un grupo o persona llego hasta el final de la ruta
	 */
	private boolean completoLaRuta;
	/**
	 * Hora de inicio de caminata representada en segundos
	 */
	private double segundosInicio;
	/**
	 * Hora de fin de caminata representada en segundos
	 */
	private double segundosFin;
	/**
	 * Hora de inicio de caminata
	 */
	private String horaInicio; //"00:00"
	/**
	 * Hora fin de caminata
	 */
	private String horaFin;	//"30:00"
	
	public PlantillaDato(int tiempoCaminandoSegundos, int cantidadHombres, int cantidadMujeres) {
		this.tiempoCaminandoSegundos = tiempoCaminandoSegundos;
		this.cantidadHombres = cantidadHombres;
		this.cantidadMujeres = cantidadMujeres;
	}

	public PlantillaDato(int numero, int tiempoCaminandoSegundos, int cantidadHombres, int cantidadMujeres,
			boolean completoLaRuta, double segundosInicio, double segundosFin, String horaInicio, String horaFin) {
		this.numero = numero;
		this.tiempoCaminandoSegundos = tiempoCaminandoSegundos;
		this.cantidadHombres = cantidadHombres;
		this.cantidadMujeres = cantidadMujeres;
		this.completoLaRuta = completoLaRuta;
		this.segundosInicio = segundosInicio;
		this.segundosFin = segundosFin;
		this.horaInicio = horaInicio;
		this.horaFin = horaFin;
	}
	
	/**
	 * Genera un vector de datos del individuo o grupo
	 * @return datos listos para mostrar en la interfaz
	 */
	public Object[] getDatos() {
		return new Object[] {this.numero, this.tiempoCaminandoSegundos, this.cantidadHombres, this.cantidadMujeres, getFormatoFinalHora(horaInicio), getFormatoFinalHora(horaFin)};
	}
	
	@Override
	public String toString() {
		return "PlantillaDato [numero=" + numero + ", tiempoCaminandoSegundos=" + tiempoCaminandoSegundos
				+ ", cantidadHombres=" + cantidadHombres + ", cantidadMujeres=" + cantidadMujeres + ", completoLaRuta="
				+ completoLaRuta + ", horaInicio=" + getFormatoFinalHora(horaInicio) + ", horaFin=" + getFormatoFinalHora(horaFin) + "]";
	}

	public int getNumero() {
		return numero;
	}

	public void setNumero(int numero) {
		this.numero = numero;
	}

	public int getTiempoCaminandoSegundos() {
		return tiempoCaminandoSegundos;
	}

	public void setTiempoCaminandoSegundos(int tiempoCaminandoSegundos) {
		this.tiempoCaminandoSegundos = tiempoCaminandoSegundos;
	}

	public int getCantidadHombres() {
		return cantidadHombres;
	}

	public void setCantidadHombres(int cantidadHombres) {
		this.cantidadHombres = cantidadHombres;
	}

	public int getCantidadMujeres() {
		return cantidadMujeres;
	}

	public void setCantidadMujeres(int cantidadMujeres) {
		this.cantidadMujeres = cantidadMujeres;
	}

	public String getHoraInicio() {
		return horaInicio;
	}

	public void setHoraInicio(String horaInicio) {
		this.horaInicio = horaInicio;
	}

	public String getHoraFin() {
		return horaFin;
	}
	
	/**
	 * Retorna la hora en el rango 10:30:00 am a 11:00:00 am
	 * @return 
	 */
	private String getFormatoFinalHora(String hora) {
		int minutos = Integer.parseInt(hora.substring(0, 2));
		if (minutos < 30) {
			return "10:" + (minutos + 30) + hora.substring(2);
		}else {
			minutos = minutos - 30;
			return "11:" + ((minutos < 10) ? "0" + minutos : minutos) + hora.substring(2);
		}
	}

	public void setHoraFin(String horaFin) {
		this.horaFin = horaFin;
	}

	public boolean isCompletoLaRuta() {
		return completoLaRuta;
	}

	public void setCompletoLaRuta(boolean completoLaRuta) {
		this.completoLaRuta = completoLaRuta;
	}

	public double getSegundosInicio() {
		return segundosInicio;
	}

	public void setSegundosInicio(double segundosInicio) {
		this.segundosInicio = segundosInicio;
	}

	public double getSegundosFin() {
		return segundosFin;
	}

	public void setSegundosFin(double segundosFin) {
		this.segundosFin = segundosFin;
	}

	@Override
	public PlantillaDato clone() throws CloneNotSupportedException {
		return (PlantillaDato) super.clone();
	}
}
