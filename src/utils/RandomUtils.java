package utils;

import java.util.Random;

/**
 * Clase que permite agregar utilidades a la clase Random de Java
 * @author Duvis Gómez && Martin Vivanco
 * @version 2.0
 */
public class RandomUtils {
	// Objeto de la clase random
	public static final Random RANDOM = new Random();

	/**
	 * Método que permite obtener valores pseudoaleatorios entre un rango de valores con un distribución uniforme
	 * @param min valor mínimo
	 * @param max valor máximo
	 * @return entero aleatorio
	 */
	public static int randomBetween(int min, int max) {
		return RANDOM.nextInt((max - min) + 1) + min;
	}
}
