package utils;

/**
 * Clase que almacena datos estandar obtenidos de acuerdo a los datos semilla de una ruta
 */
public class DatosGenerales {
	/**
	 * probabilidad de que un dato de la ruta sea grupo (numero entre 0 y 100)
	 */
	private double probabilidadDeSerGrupo;
	/**
	 * probabilidad de que un dato de la ruta, la haya completado (numero entre 0 y 100)
	 */
	private double probabilidadCompleto;
	
	public DatosGenerales(double probabilidadDeSerGrupo, double probabilidadCompleto) {
		this.probabilidadDeSerGrupo = probabilidadDeSerGrupo;
		this.probabilidadCompleto = probabilidadCompleto;
	}

	public double getProbabilidadCompleto() {
		return probabilidadCompleto;
	}

	public void setProbabilidadCompleto(double probabilidadCompleto) {
		this.probabilidadCompleto = probabilidadCompleto;
	}

	public double getProbabilidadDeSerGrupo() {
		return probabilidadDeSerGrupo;
	}

	public void setProbabilidadDeSerGrupo(double probabilidadDeSerGrupo) {
		this.probabilidadDeSerGrupo = probabilidadDeSerGrupo;
	}
}
