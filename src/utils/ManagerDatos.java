package utils;

import java.util.ArrayList;

/**
 * Esta clase almacena los datos semilla, asi como algunos datos 
 */
public class ManagerDatos {
	/**
	 * Datos semilla del tramo 1
	 */
    protected ArrayList<PlantillaDato> tramo1 = new ArrayList<>();
    /**
	 * Datos Generales del tramo 1, probabilidad de ser grupo y probabilidad de completar la ruta
	 */
    protected DatosGenerales datosGeneralesTramo1 = new DatosGenerales((7 * 100) / 16, 100);
    /**
     * Datos semilla del tramo 2
     */
    protected ArrayList<PlantillaDato> tramo2 = new ArrayList<>();
    /**
	 * Datos Generales del tramo 2, probabilidad de ser grupo y probabilidad de completar la ruta
	 */
    protected DatosGenerales datosGeneralesTramo2 = new DatosGenerales(50, 75);
    /**
	 * Datos semilla del tramo 3
	 */
    protected ArrayList<PlantillaDato> tramo3 = new ArrayList<>();
    /**
	 * Datos Generales del tramo 3, probabilidad de ser grupo y probabilidad de completar la ruta
	 */
    protected DatosGenerales datosGeneralesTramo3 = new DatosGenerales(50, 100);
    /**
	 * Datos semilla del tramo 4
	 */
    protected ArrayList<PlantillaDato> tramo4 = new ArrayList<>();
    /**
	 * Datos Generales del tramo 4, probabilidad de ser grupo y probabilidad de completar la ruta
	 */
    protected DatosGenerales datosGeneralesTramo4 = new DatosGenerales(60, 60);

    /**
     * Se crean losd datos semilla de forma "quemada" en codigo
     */
    public ManagerDatos() {
    	// Primer Lunes
        tramo1.add(new PlantillaDato(131, 1, 1));
        tramo1.add(new PlantillaDato(115, 2, 0));
        tramo1.add(new PlantillaDato(109, 1, 0));
        tramo1.add(new PlantillaDato(167, 1, 0));
        tramo1.add(new PlantillaDato(126, 0, 1));
        tramo1.add(new PlantillaDato(170, 0, 1));
        tramo1.add(new PlantillaDato(131, 1, 1));
        tramo1.add(new PlantillaDato(76, 1, 1));
        tramo1.add(new PlantillaDato(118, 1, 0));
        tramo1.add(new PlantillaDato(220, 0, 1));
        tramo1.add(new PlantillaDato(133, 3, 0));
        tramo1.add(new PlantillaDato(140, 0, 1));
        tramo1.add(new PlantillaDato(146, 1, 1));
        tramo1.add(new PlantillaDato(106, 1, 0));
        tramo1.add(new PlantillaDato(135, 1, 1));
        tramo1.add(new PlantillaDato(152, 1, 0));

    	// Primer Lunes
        tramo2.add(new PlantillaDato(206, 1, 2));
        tramo2.add(new PlantillaDato(65, 1, 0));
    	// Segundo Lunes
        tramo2.add(new PlantillaDato(232, 0, 2));
        tramo2.add(new PlantillaDato(71, 1, 0));

    	// Primer Lunes
        tramo3.add(new PlantillaDato(128, 1, 2));
        tramo3.add(new PlantillaDato(147, 1, 0));
    	// Segundo Lunes
        tramo3.add(new PlantillaDato(177, 0, 2));
        tramo3.add(new PlantillaDato(56, 1, 0));

    	// Primer Lunes
        tramo4.add(new PlantillaDato(107, 1, 0));
        tramo4.add(new PlantillaDato(130, 1, 2));
        tramo4.add(new PlantillaDato(116, 2, 0));
        tramo4.add(new PlantillaDato(15, 1, 0));
        tramo4.add(new PlantillaDato(97, 2, 0));
        tramo4.add(new PlantillaDato(70, 1, 0));
    	// Segundo Lunes
        tramo4.add(new PlantillaDato(150, 0, 2));
        tramo4.add(new PlantillaDato(40, 0, 2));
        tramo4.add(new PlantillaDato(30, 1, 0));
        tramo4.add(new PlantillaDato(102, 0, 2));
    }

    /**
     * Metodo para obtener el tiempo (en segundos) promedio, de todas las personas de un tramo
     * @param datos personas del tramo
     * @return el tiempo promedio de los tiempo de caminata
     */
    public double getPromerdioTiempo(ArrayList<PlantillaDato> datos) {
        double tiempo = 0;
        for (PlantillaDato plantillaDato : datos) {
            tiempo += plantillaDato.getTiempoCaminandoSegundos();
        }
        return tiempo / datos.size();
    }

    /**
     * Genera la desviacion estandar de los tiempos de caminata de un tramo
     * @param datos personas del tramo
     * @return la desviacion estandar de los tiempos de la caminata de un tramo
     */
    public double getDesviacionEstandarTiempo(ArrayList<PlantillaDato> datos) {
        double sumatoria = 0;
        double promedio = getPromerdioTiempo(datos);
        for (PlantillaDato plantillaDato : datos) {
            sumatoria += Math.pow((plantillaDato.getTiempoCaminandoSegundos() - promedio), 2);
        }
        return Math.sqrt(sumatoria / (datos.size() - 1));
    }

    /**
     * Permite obtener el menor tiempo de caminata de un tramo
     * @param tramo personas del tramo
     * @return el menor tiempo de caminata
     */
    public int getTiempoMenor(ArrayList<PlantillaDato> tramo) {
        int tiempo = Integer.MAX_VALUE;
        for (PlantillaDato plantillaDato : tramo) {
            if (plantillaDato.getTiempoCaminandoSegundos() < tiempo) {
                tiempo = plantillaDato.getTiempoCaminandoSegundos();
            }
        }
        return tiempo;
    }

    /**
     * Permite obtener el mayor tiempo de caminata de un tramo
     * @param tramo personas del tramo
     * @return el mayor tiempo de caminata
     */
    public int getTiempoMayor(ArrayList<PlantillaDato> tramo) {
        int tiempo = Integer.MIN_VALUE;
        for (PlantillaDato plantillaDato : tramo) {
            if (plantillaDato.getTiempoCaminandoSegundos() > tiempo) {
                tiempo = plantillaDato.getTiempoCaminandoSegundos();
            }
        }
        return tiempo;
    }

    /**
     * Metodo para obtener el numero de hombres promedio, de todas las personas de un tramo
     * @param datos personas del tramo
     * @return el numero de hombres promedio de los tiempo de caminata
     */
    public double getPromedioCantidadHombres(ArrayList<PlantillaDato> datos) {
        double cantidad = 0;
        for (PlantillaDato plantillaDato : datos) {
            cantidad += plantillaDato.getCantidadHombres();
        }
        return cantidad / datos.size();
    }

    /**
     * Metodo para obtener el numero de mujeres promedio, de todas las personas de un tramo
     * @param datos personas del tramo
     * @return el numero de mujeres promedio de los tiempo de caminata
     */
    public double getPromedioCantidadMujeres(ArrayList<PlantillaDato> datos) {
        double cantidad = 0;
        for (PlantillaDato plantillaDato : datos) {
            cantidad += plantillaDato.getCantidadMujeres();
        }
        return cantidad / datos.size();
    }

    /**
     * Genera la desviacion estandar de los hombres que pasaron por un tramo
     * @param datos personas del tramo
     * @return la desviacion estandar los hombres que pasaron por un tramo
     */
    public double getDesviacionCantidadHombres(ArrayList<PlantillaDato> datos) {
        double sumatoria = 0;
        double promedio = getPromedioCantidadHombres(datos);        
        for (PlantillaDato plantillaDato : datos) {            
            sumatoria += Math.pow((plantillaDato.getCantidadHombres()- promedio), 2);
        }
        return Math.sqrt(sumatoria / (datos.size() - 1));
    }

    /**
     * Genera la desviacion estandar de las mujeres que pasaron por un tramo
     * @param datos personas del tramo
     * @return la desviacion estandar las mujeres que pasaron por un tramo
     */
    public double getDesviacionCantidadMujeres(ArrayList<PlantillaDato> datos) {
        double sumatoria = 0;
        double promedio = getPromedioCantidadHombres(datos);        
        for (PlantillaDato plantillaDato : datos) {            
            sumatoria += Math.pow((plantillaDato.getCantidadMujeres() - promedio), 2);
        }
        return Math.sqrt(sumatoria / (datos.size() - 1));
    }
    
    /**
     * Obtiene la menor cantidad de hombres entre todos los grupos de una ruta
     * @param tramo personas del tramo
     * @return menor cantidad de hombres de todos los grupos
     */
    public int getCantidadMenorHombres(ArrayList<PlantillaDato> tramo) {
        int cantidad = Integer.MAX_VALUE;
        for (PlantillaDato plantillaDato : tramo) {
            if (plantillaDato.getCantidadHombres() < cantidad) {
                cantidad = plantillaDato.getCantidadHombres();
            }
        }
        return cantidad;
    }

    /**
     * Obtiene la mayor cantidad de hombres entre todos los grupos de una ruta
     * @param tramo personas del tramo
     * @return mayor cantidad de hombres de todos los grupos
     */
    public int getCantidadMayorHombres(ArrayList<PlantillaDato> tramo) {
        int cantidad = Integer.MIN_VALUE;
        for (PlantillaDato plantillaDato : tramo) {
            if (plantillaDato.getCantidadHombres() > cantidad) {
                cantidad = plantillaDato.getCantidadHombres();
            }
        }
        return cantidad;
    }
    
    /**
     * Obtiene la menor cantidad de mujeres entre todos los grupos de una ruta
     * @param tramo personas del tramo
     * @return menor cantidad de mujeres de todos los grupos
     */
    public int getCantidadMenorMujeres(ArrayList<PlantillaDato> tramo) {
        int cantidad = Integer.MAX_VALUE;
        for (PlantillaDato plantillaDato : tramo) {
            if (plantillaDato.getCantidadMujeres() < cantidad) {
                cantidad = plantillaDato.getCantidadMujeres();
            }
        }
        return cantidad;
    }

    /**
     * Obtiene la mayor cantidad de mujeres entre todos los grupos de una ruta
     * @param tramo personas del tramo
     * @return mayor cantidad de mujeres de todos los grupos
     */
    public int getCantidadMayorMujeres(ArrayList<PlantillaDato> tramo) {
        int tiempo = Integer.MIN_VALUE;
        for (PlantillaDato plantillaDato : tramo) {
            if (plantillaDato.getCantidadMujeres() > tiempo) {
                tiempo = plantillaDato.getCantidadMujeres();
            }
        }
        return tiempo;
    }
}
