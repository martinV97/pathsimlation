package utils.scenaryOne;

import java.util.ArrayList;
import java.util.concurrent.atomic.AtomicInteger;

public class Route {


	private final int id;
	private static final AtomicInteger count = new AtomicInteger(0); 
		
	private EnumRoute numberRoute;
	private int tiempoInicial;
	private int tiempoFinal;
	private Boolean enRuta =  false;
	private Boolean completo;
	private ArrayList<Tiempo> tiempoTotal;

	public Route() {
		this.id = count.incrementAndGet();
	}

	public Route(EnumRoute numberRoute, int tiempoInicial, int tiempoFinal) {
		this.id = count.incrementAndGet();
		tiempoTotal =  new ArrayList<Tiempo>();
		this.numberRoute = numberRoute;
		this.tiempoInicial = tiempoInicial;
		this.tiempoFinal = tiempoFinal;
		if(numberRoute.getValue() == 1) {
			tiempoTotal.add(new Tiempo(numberRoute, getDeltaTiempo()));
		}
		completo =  true;
	}

	public EnumRoute getNumberRoute() {
		return numberRoute;
	}

	public void setNumberRoute(EnumRoute numberRoute) {
		this.numberRoute = numberRoute;
	}

	public int getTiempoInicial() {
		return tiempoInicial;
	}

	public void setTiempoInicial(int tiempoInicial) {
		this.tiempoInicial = tiempoInicial;
	}

	public int getTiempoFinal() {
		return tiempoFinal;
	}

	public void setTiempoFinal(int tiempoFinal) {
		this.tiempoFinal = tiempoFinal;
	}

	public Boolean getCompleto() {
		return completo;
	}

	public void setCompleto(Boolean completo) {
		this.completo = completo;
	}

	public int getDeltaTiempo() {
		return tiempoFinal - tiempoInicial;
	}

	public void addTiempo (Tiempo tiempo) {
		tiempoTotal.add(tiempo);
	}
	
	public Boolean getEnRuta() {
		return enRuta;
	}

	public void setEnRuta(Boolean enRuta) {
		this.enRuta = enRuta;
	}

	public ArrayList<Tiempo> getTiempoTotal() {
		return tiempoTotal;
	}

	public void setTiempoTotal(ArrayList<Tiempo> tiempoTotal) {
		this.tiempoTotal = tiempoTotal;
	}

	public int getId() {
		return id;
	}

	@Override
	public String toString() {
		return "Route [id=" + id + ", numberRoute=" + numberRoute + ", tiempoInicial=" + tiempoInicial
				+ ", tiempoFinal=" + tiempoFinal + ", enRuta=" + enRuta + ", completo=" + completo + ", tiempoTotal="
				+ tiempoTotal + "]";
	}

	public void cambiarDeEstado() {
		if(completo) {
			completo =  false;
		}else {
			completo = true;
		}
	}
	
	public int  getDeltaAcumulado() {
		int delta = 0 ;
		for (int i = 0; i < tiempoTotal.size(); i++) {
			delta +=  tiempoTotal.get(i).getTiempo();
		}
		return delta;
	}

	

	
}
