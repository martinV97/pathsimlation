package utils.scenaryOne;

public class Tiempo {
	
	private EnumRoute enumRoute;
	private int tiempo;
	
	public Tiempo(EnumRoute enumRoute, int tiempo) {
		this.enumRoute = enumRoute;
		this.tiempo = tiempo;
	}

	public EnumRoute getEnumRoute() {
		return enumRoute;
	}

	public void setEnumRoute(EnumRoute enumRoute) {
		this.enumRoute = enumRoute;
	}

	public int getTiempo() {
		return tiempo;
	}

	public void setTiempo(int tiempo) {
		this.tiempo = tiempo;
	}

	@Override
	public String toString() {
		return "Tiempo [enumRoute=" + enumRoute + ", tiempo=" + tiempo + "]";
	}
	
	
	
	
}
