package utils.scenaryOne;

public enum EnumRoute {
	
	ONE(1), TWO(2),THREE(3), FOUR(4);

	private final int value;
  
    EnumRoute(int value) {
        this.value = value;
    }

    public int getValue() {
        return value;
    }

}
