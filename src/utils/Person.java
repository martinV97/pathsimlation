package utils;

import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;
import java.util.Date;

public class Person {
	private int numberOfPerson;
	private char gender;
	private String initialTime; // "00:00"
	private String finalTime; // "30:00"
	private int duration;
	private boolean isCompleted;

	public Person(int numberOfPerson, char gender, String initialTime) {
		this.numberOfPerson = numberOfPerson;
		this.gender = gender;
		this.initialTime = initialTime;
	}

	/**
	 * Método que obtiene la duración en minutos entre el initialTime y el finalTime
	 * 
	 * @return
	 */
	public double getDurationMinutes() {
		String[] auxTimeinitialTime = initialTime.split(":");
		int hourInitial = Integer.parseInt(auxTimeinitialTime[0]);
		int minuteInitial = Integer.parseInt(auxTimeinitialTime[1]);

		String[] auxTimefinalTime = finalTime.split(":");
		int hourFinal = Integer.parseInt(auxTimefinalTime[0]);
		int minuteFinal = Integer.parseInt(auxTimefinalTime[1]);

		LocalDateTime initialTime = LocalDateTime.of(2019, 06, 30, hourInitial, minuteInitial, 0);
		LocalDateTime finalTime = LocalDateTime.of(2019, 06, 30, hourFinal, minuteFinal, 0);
		return ChronoUnit.MINUTES.between(initialTime, finalTime);
	}

	/**
	 * @return  Método que obtiene el valor de la propiedad duration.
	 */
	public int getDuration() {
		return duration;
	}

	/**
	 * Método que permite obtener la fecha final a partir de una fecha inicial y una
	 * duración.
	 * 
	 * @param initialTime
	 * @param duration    Duración del evento en minutos
	 * @return
	 */
	public String getFinalTime(String initialTime, int duration) {
		String[] auxTimeinitialTime = initialTime.split(":");
		int hourInitial = Integer.parseInt(auxTimeinitialTime[0]);
		int minuteInitial = Integer.parseInt(auxTimeinitialTime[1]);
		LocalDateTime initialTimeDate = LocalDateTime.of(2019, 06, 30, hourInitial, minuteInitial, 0);

		long milliSeconds = Timestamp.valueOf(initialTimeDate).getTime();
		DateFormat simple = new SimpleDateFormat("dd MMM yyyy HH:mm:ss:SSS Z");
		Date result = new Date(milliSeconds + (duration * 60000));
		return simple.format(result).substring(12, 17);
	}

	/**
	 * @return Método que obtiene el valor de la propiedad numberOfPerson.
	 */
	public int getNumberOfPerson() {
		return numberOfPerson;
	}

	/**
	 * @param Método que asigna el valor de la propiedad numberOfPerson.
	 */
	public void setNumberOfPerson(int numberOfPerson) {
		this.numberOfPerson = numberOfPerson;
	}

	/**
	 * @return Método que obtiene el valor de la propiedad gender.
	 */
	public char getGender() {
		return gender;
	}

	/**
	 * @param Método que asigna el valor de la propiedad gender.
	 */
	public void setGender(char gender) {
		this.gender = gender;
	}

	/**
	 * @return Método que obtiene el valor de la propiedad initialTime.
	 */
	public String getInitialTime() {
		return initialTime;
	}

	/**
	 * @param Método que asigna el valor de la propiedad initialTime.
	 */
	public void setInitialTime(String initialTime) {
		this.initialTime = initialTime;
	}

	/**
	 * @return Método que obtiene el valor de la propiedad finalTime.
	 */
	public String getFinalTime() {
		return finalTime;
	}

	/**
	 * @param Método que asigna el valor de la propiedad finalTime.
	 */
	public void setFinalTime(String finalTime) {
		this.finalTime = finalTime;
	}

	/**
	 * @return Método que obtiene el valor de la propiedad isCompleted.
	 */
	public boolean isCompleted() {
		return isCompleted;
	}

	/**
	 * @param Método que asigna el valor de la propiedad isCompleted.
	 */
	public void setCompleted(boolean isCompleted) {
		this.isCompleted = isCompleted;
	}

	/**
	 * @param Método que asigna el valor de la propiedad duration.
	 */
	public void setDuration(int duration) {
		this.duration = duration;
	}

}
