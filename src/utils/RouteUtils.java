package utils;

/**
 * 
 * @author Duvis Gómez && Martin Vivanco
 * @version 1.0
 */
public class RouteUtils {
	private Route route1;
	private Route route2;
	private Route route3;
	private Route route4;
	
	public RouteUtils() {
		route1 = new Route("Route 1");
		route2 = new Route("Route 2");
		route3 = new Route("Route 3");
		route4 = new Route("Route 4");
	}
	
	/**
	 * @return  Método que obtiene el valor de la propiedad route1.
	 */
	public Route getRoute1() {
		return route1;
	}
	/**
	 * @param Método que asigna el valor de la propiedad route1.
	 */
	public void setRoute1(Route route1) {
		this.route1 = route1;
	}
	/**
	 * @return  Método que obtiene el valor de la propiedad route2.
	 */
	public Route getRoute2() {
		return route2;
	}
	/**
	 * @param Método que asigna el valor de la propiedad route2.
	 */
	public void setRoute2(Route route2) {
		this.route2 = route2;
	}
	/**
	 * @return  Método que obtiene el valor de la propiedad route3.
	 */
	public Route getRoute3() {
		return route3;
	}
	/**
	 * @param Método que asigna el valor de la propiedad route3.
	 */
	public void setRoute3(Route route3) {
		this.route3 = route3;
	}
	/**
	 * @return  Método que obtiene el valor de la propiedad route4.
	 */
	public Route getRoute4() {
		return route4;
	}
	/**
	 * @param Método que asigna el valor de la propiedad route4.
	 */
	public void setRoute4(Route route4) {
		this.route4 = route4;
	}
}
