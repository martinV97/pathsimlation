package controller;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import logic.scenaryOne.ScenaryOne;
import utils.scenaryOne.EnumRoute;
import utils.scenaryOne.Route;
import view.MainWindows;

public class ControllerEscenario1 implements ActionListener{
	
	private MainWindows mainWindow;
	private ScenaryOne scenaryOne;
	
	
	public ControllerEscenario1() {
		ArrayList<Route> datosRutaOne =  new ArrayList<Route>();
		datosRutaOne.add(new Route(EnumRoute.ONE, 60, 180));
		datosRutaOne.add(new Route(EnumRoute.ONE, 240, 420));
		datosRutaOne.add(new Route(EnumRoute.ONE, 360, 660));
		datosRutaOne.add(new Route(EnumRoute.ONE, 420, 580));
		datosRutaOne.add(new Route(EnumRoute.ONE, 480, 690));
		datosRutaOne.add(new Route(EnumRoute.ONE, 540, 730));
		datosRutaOne.add(new Route(EnumRoute.ONE, 720, 900));
		datosRutaOne.add(new Route(EnumRoute.ONE, 780, 870));
		datosRutaOne.add(new Route(EnumRoute.ONE, 840, 1030));
		datosRutaOne.add(new Route(EnumRoute.ONE, 840, 1050));
		datosRutaOne.add(new Route(EnumRoute.ONE, 900, 1100));
		datosRutaOne.add(new Route(EnumRoute.ONE, 1080, 1353));
		datosRutaOne.add(new Route(EnumRoute.ONE, 1140, 1440));
		datosRutaOne.add(new Route(EnumRoute.ONE, 1380, 1620));
		datosRutaOne.add(new Route(EnumRoute.ONE, 1500, 1620));
		
		//Datos ruta 3
		ArrayList<Route> datosRutaTwo =  new ArrayList<Route>();
		datosRutaTwo.add(new Route(EnumRoute.TWO, 0, 232));
		datosRutaTwo.add(new Route(EnumRoute.TWO, 1407, 1478));
		
		//Datos ruta 3
		ArrayList<Route> datosRutaTheee =  new ArrayList<Route>();
		datosRutaTheee.add(new Route(EnumRoute.THREE, 60, 295));
		datosRutaTheee.add(new Route(EnumRoute.THREE, 1380, 1436));
		
		//Datos ruta 4
		ArrayList<Route> datosRutaFour =  new ArrayList<Route>();
		datosRutaFour.add(new Route(EnumRoute.FOUR, 250, 400));
		datosRutaFour.add(new Route(EnumRoute.FOUR, 1328, 1350));
		datosRutaFour.add(new Route(EnumRoute.FOUR, 1380, 1442));
		
		// Siulacion
		scenaryOne = new ScenaryOne();
		mainWindow =  new MainWindows(this);
		scenaryOne.setRutaUno(datosRutaOne);
		scenaryOne.setAuxRuta1(datosRutaOne);
		scenaryOne.setRutaDos(datosRutaTwo);
		scenaryOne.setRutaTres(datosRutaTheee);
		scenaryOne.setRutaCuatro(datosRutaFour);
		
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		switch (Action.valueOf(e.getActionCommand())) {
		case RUTA_1:
			mainWindow.addDataToTableRuta(scenaryOne.getRutaUnoArray(),
					scenaryOne.promedioTiemposEnSegundos(scenaryOne.getRutaUno()));
			break;
		case RUTA_2:
			mainWindow.addDataToTableRuta(scenaryOne.getRutaDosArray(),
					scenaryOne.promedioTiemposEnSegundos(scenaryOne.getRutaDos()));
			break;
		case RUTA_3:
			mainWindow.addDataToTableRuta(scenaryOne.getRutaTresArray(), 
					scenaryOne.promedioTiemposEnSegundos(scenaryOne.getRutaTres()));
			break;
		case RUTA_4:
			mainWindow.addDataToTableRuta(scenaryOne.getRutaCuatroArray(), 
					scenaryOne.promedioTiemposEnSegundos(scenaryOne.getRutaCuatro()));
			break;
		case RUTA_TOTAL:
			scenaryOne.run(scenaryOne.getRutaUno());
			scenaryOne.run(scenaryOne.getRutaDos());
			scenaryOne.run(scenaryOne.getRutaTres());
			scenaryOne.run(scenaryOne.getRutaCuatro());
			mainWindow.addDataToTableRutaTotal(scenaryOne.getRutaTotalArray(), scenaryOne.promedioTiempoRutaCompleta());
			break;
		}
	}

	public static void main(String[] args) {
		new ControllerEscenario1();
		
	}
}
