package logic.scenaryOne;

import java.util.ArrayList;
import java.util.Collections;

import utils.scenaryOne.Route;
import utils.scenaryOne.Tiempo;

public class ScenaryOne {

	private ArrayList<Route> rutaUno;
	private ArrayList<Route> rutaDos;
	private ArrayList<Route> rutaTres;
	private ArrayList<Route> rutaCuatro;
	private ArrayList<Route> auxRuta1;

	public ArrayList<Route> getRutaUno() {
		return rutaUno;
	}

	public void setRutaUno(ArrayList<Route> rutaUno) {
		this.rutaUno = rutaUno;
	}
	
	public void setAuxRuta1(ArrayList<Route> auxRuta1) {
		this.auxRuta1 =  new ArrayList<Route>();
		for (int i = 0; i < auxRuta1.size(); i++) {
			this.auxRuta1.add(auxRuta1.get(i));
		}
		
	}

	public Double promedioTiemposEnSegundos(ArrayList<Route> ruta) {
		int totalTiempo = 0;
		for (int i = 0; i < ruta.size(); i++) {
			totalTiempo += ruta.get(i).getDeltaTiempo();
		}
		return (double) (totalTiempo / ruta.size());
	}

	public void imprimirInfo(ArrayList<Route> ruta) {
		for (int i = 0; i < ruta.size(); i++) {
			System.out.println(ruta.get(i).toString());
		}
	}

	public ArrayList<Route> getRutaDos() {
		return rutaDos;
	}

	public void setRutaDos(ArrayList<Route> rutaDos) {
		this.rutaDos = rutaDos;
	}

	public ArrayList<Route> getRutaTres() {
		return rutaTres;
	}

	public void setRutaTres(ArrayList<Route> rutaTres) {
		this.rutaTres = rutaTres;
	}

	public ArrayList<Route> getRutaCuatro() {
		return rutaCuatro;
	}

	public void setRutaCuatro(ArrayList<Route> rutaCuatro) {
		this.rutaCuatro = rutaCuatro;
	}

	
	/**
	 * Método que ejecuta obtiene que tiempos continuan en la ruta
	 * 
	 */
	public void run(ArrayList<Route> entrada) {
		Collections.shuffle(rutaUno);
		ArrayList<Route> aux = new ArrayList<Route>();
		if (entrada.size() <= rutaUno.size()) {
			for (int i = 0; i < entrada.size(); i++) {
				for (int j = 0; j < rutaUno.size(); j++) {
					if (rutaUno.get(j).getCompleto() &&  !rutaUno.get(j).getEnRuta()) {
						rutaUno.get(j).getTiempoTotal()
								.add(new Tiempo(entrada.get(i).getNumberRoute(), entrada.get(i).getDeltaTiempo()));
						rutaUno.get(j).setEnRuta(true);
						break;
					}
				}
			}
			for (int j = 0; j < rutaUno.size(); j++) {
				if(!rutaUno.get(j).getEnRuta()) {
					rutaUno.get(j).setCompleto(false);
				}
			}
			for (int j = 0; j < rutaUno.size(); j++) {
					rutaUno.get(j).setEnRuta(false);
			}
			
		} else {
			for (int j = 0; j < rutaUno.size(); j++) {
				if (rutaUno.get(j).getCompleto() && !rutaUno.get(j).getEnRuta()) {
					rutaUno.get(j).getTiempoTotal().add(new Tiempo(entrada.get(j).getNumberRoute(), entrada.get(j).getDeltaTiempo()));
					rutaUno.get(j).setEnRuta(true);
				}

			}
			for (int j = 0; j < rutaUno.size(); j++) {
				if(!rutaUno.get(j).getEnRuta()) {
					rutaUno.get(j).setCompleto(false);
				}
			}
			for (int j = 0; j < rutaUno.size(); j++) {
				rutaUno.get(j).setEnRuta(false);
			}
		}
	}
	
	/**
	 * Método que retorna el promedio del tiempo de todos los tiempos que cumpnimaron las 4 rutas
	 * @return
	 */
	public int promedioTiempoRutaCompleta() {
		int x = 0;
		int promedio= 0;
		for (int i = 0; i < rutaUno.size(); i++) {
			if(rutaUno.get(i).getCompleto()) {
				x++;
				promedio += rutaUno.get(i).getDeltaAcumulado();
			}
		}
		return promedio / x;
	}
	
	/**
	 * Método que retorna la lista de tiempos que completaron la ruta
	 * @return
	 */
	
	public ArrayList<Route> getTiemposQueCumplieronLaRutaCompleta(){
		ArrayList<Route> tiemposQueCumplieronLaRutaCompleta =  new ArrayList<Route>();
		for (int i = 0; i < rutaUno.size(); i++) {
			if(rutaUno.get(i).getCompleto()) {
				tiemposQueCumplieronLaRutaCompleta.add(rutaUno.get(i));
			}
		}
		return tiemposQueCumplieronLaRutaCompleta;
	}
	
	public ArrayList<Object[]> getRutaUnoArray(){
		ArrayList<Object[]> salida =new ArrayList<Object[]>();
		for (int i = 0; i < auxRuta1.size(); i++) {
			Object [] aux = {auxRuta1.get(i).getId(), auxRuta1.get(i).getTiempoInicial(), auxRuta1.get(i).getTiempoFinal(),
					auxRuta1.get(i).getDeltaTiempo(), auxRuta1.get(i).getCompleto()? "si" : "no"};
			salida.add(aux);
		}
		return salida;
	}

	public ArrayList<Object[]> getRutaDosArray() {
		ArrayList<Object[]> salida =new ArrayList<Object[]>();
		for (int i = 0; i < rutaDos.size(); i++) {
			Object [] aux = {rutaDos.get(i).getId(), rutaDos.get(i).getTiempoInicial(), rutaDos.get(i).getTiempoFinal(),
					rutaDos.get(i).getDeltaTiempo(), rutaDos.get(i).getCompleto()? "si" : "no"};
			salida.add(aux);
		}
		return salida;
	}

	public ArrayList<Object[]> getRutaTresArray() {
		ArrayList<Object[]> salida =new ArrayList<Object[]>();
		for (int i = 0; i < rutaTres.size(); i++) {
			Object [] aux = {rutaTres.get(i).getId(), rutaTres.get(i).getTiempoInicial(), rutaTres.get(i).getTiempoFinal(),
					rutaTres.get(i).getDeltaTiempo(), rutaTres.get(i).getCompleto()? "si" : "no"};
			salida.add(aux);
		}
		return salida;
	}

	public ArrayList<Object[]> getRutaCuatroArray() {
		ArrayList<Object[]> salida =new ArrayList<Object[]>();
		for (int i = 0; i < rutaTres.size(); i++) {
			Object [] aux = {rutaCuatro.get(i).getId(), rutaCuatro.get(i).getTiempoInicial(), rutaCuatro.get(i).getTiempoFinal(),
					rutaCuatro.get(i).getDeltaTiempo(), rutaCuatro.get(i).getCompleto()? "si" : "no"};
			salida.add(aux);
		}
		return salida;
	}

	public ArrayList<Object[]> getRutaTotalArray() {
		ArrayList<Route> rutasTotal = getTiemposQueCumplieronLaRutaCompleta(); 
		ArrayList<Object[]> salida =new ArrayList<Object[]>();
		for (int i = 0; i < rutasTotal.size(); i++) {
			Object [] aux = {rutasTotal.get(i).getId(),
					rutasTotal.get(i).getTiempoTotal().get(0).getTiempo(), 
					rutasTotal.get(i).getTiempoTotal().get(1).getTiempo(),
					rutasTotal.get(i).getTiempoTotal().get(2).getTiempo(),
					rutasTotal.get(i).getTiempoTotal().get(3).getTiempo(),
					rutasTotal.get(i).getDeltaAcumulado(),
					rutasTotal.get(i).getCompleto()? "si" : "no"};
			salida.add(aux);
		}
		return salida;
	}

}
