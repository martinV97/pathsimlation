package
logic.scenaryThree;

import java.util.ArrayList;

import utils.Person;

public interface InterfaceScenaryThree {
	/**
	 * Método que retorna el promedio del tiempo de la duración del recorrido de las personas
	 * en la ruta 1. 
	 * @return
	 */
	public double getAverageRoute1();
	/**
	 * Método que retorna el promedio del tiempo de la duración del recorrido de las personas
	 * en la ruta 2.
	 * @return
	 */
	public double getAverageRoute2();
	/**
	 * Método que retorna el promedio del tiempo de la duración del recorrido de las personas
	 * en la ruta 3.
	 * @return
	 */
	public double getAverageRoute3();
	/**
	 * Método que retorna el promedio del tiempo de la duración del recorrido de las personas
	 * en la ruta 4.
	 * @return
	 */
	public double getAverageRoute4();
	/**
	 * Método que retorna la lista de las personas que recorrieron la ruta 1.
	 * @return
	 */
	public ArrayList<Person> getPersonsOfRouteOne();
	/**
	 * Método que retorna la lista de las personas que recorrieron la ruta 2.
	 * @return
	 */
	public ArrayList<Person> getPersonsOfRouteTwo();
	/**
	 * Método que retorna la lista de las personas que recorrieron la ruta 3.
	 * @return
	 */
	public ArrayList<Person> getPersonsOfRouteThree();
	/**
	 * Método que retorna la lista de las personas que recorrieron la ruta 4.
	 * @return
	 */
	public ArrayList<Person> getPersonsOfRouteFour();
	/**
	 * Método que retorna la lista de todas las personas que recorrieron las 4 rutas en el escenario 3.
	 * @return
	 */
	public ArrayList<Person> getAllPersonsScenaryThree();
	/**
	 * Método que retorna la cantidad de lunes necesarios para que transiten las personas ingresadas por parametro
	 * @return
	 */
	public int getAmountOfMondays();
}
