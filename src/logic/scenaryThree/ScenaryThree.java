package logic.scenaryThree;

import java.util.ArrayList;

import utils.Person;
import utils.RandomUtils;
import utils.Route;
import utils.RouteUtils;

/**
 * Clase que permite realizar la simulación del escenario 3.
 * 
 * @author Duvis Gómez && Martin Vivanco
 * @version 1.0
 */
public class ScenaryThree implements InterfaceScenaryThree {
	private RouteUtils routeUtils;
	private Route route1;
	private Route route2;
	private Route route3;
	private Route route4;

	private int quantityPersons, hoursPerDay;

	/**
	 * Constructor de la clase escenario 3 en la que se inicia la simulación,
	 * basada en los datos recopilados.
	 * @param quantityPersons Cantidad de personas que desean pasar una determinada
	 *                        ruta.
	 * @param hoursPerDay     Tiempo al día en que una persona puede realizar una
	 *                        ruta.
	 */
	public ScenaryThree(int quantityPersons, int hoursPerDay) {
		initializeRoutes();

		// 131 personas por hora en las 4 rutas
		// Ruta 1 --> 78.6% de las personas Completan 35.9% NC 64.1% 51%H 49%M
		// Ruta 2 --> 5.3% de las personas Completan 85% NC 15% 42%H 58%M
		// Ruta 3 --> 5.3% de las personas. Completan 98% NC 2% 42%H 58%M
		// Ruta 4 --> 10.6% de las personas Completan 71% NC 29% 80%H 20%M
		for (int i = 0; i < quantityPersons; i++) {
			int montecarloRoutes = RandomUtils.randomBetween(1, 100);
			int auxMinutes = RandomUtils.randomBetween(30, 59);
			if (montecarloRoutes < 79) {
				int montecarloCompleteRoute = RandomUtils.randomBetween(1, 100);
				int gender = RandomUtils.randomBetween(1, 100);
				char charGender = ' ';
				if (gender < 51) {
					charGender = 'H';
				} else {
					charGender = 'M';
				}
				Person person = new Person((i + 1), charGender, "10:" + auxMinutes);
				person.setDuration(RandomUtils.randomBetween(route1.getTimeMin(), route1.getTimeMax()));
				person.setFinalTime(person.getFinalTime(person.getInitialTime(), person.getDuration()));
				person.setCompleted(montecarloCompleteRoute < 36);
				this.route1.getListHistoricalPeople().add(person);
			} else if (montecarloRoutes < 85) {
				int montecarloCompleteRoute = RandomUtils.randomBetween(1, 100);
				char charGender = ' ';
				int gender = RandomUtils.randomBetween(1, 100);
				if (gender < 42) {
					charGender = 'H';
				} else {
					charGender = 'M';
				}
				Person person = new Person((i + 1), charGender, "10:" + auxMinutes);
				person.setDuration(RandomUtils.randomBetween(route2.getTimeMin(), route2.getTimeMax()));
				person.setFinalTime(person.getFinalTime(person.getInitialTime(), person.getDuration()));
				person.setCompleted(montecarloCompleteRoute < 85);
				this.route2.getListHistoricalPeople().add(person);
			} else if (montecarloRoutes < 90) {
				int montecarloCompleteRoute = RandomUtils.randomBetween(1, 100);
				int gender = RandomUtils.randomBetween(1, 100);
				char charGender = ' ';
				if (gender < 42) {
					charGender = 'H';
				} else {
					charGender = 'M';
				}
				Person person = new Person((i + 1), charGender, "10:" + auxMinutes);
				person.setDuration(RandomUtils.randomBetween(route3.getTimeMin(), route3.getTimeMax()));
				person.setFinalTime(person.getFinalTime(person.getInitialTime(), person.getDuration()));
				person.setCompleted(montecarloCompleteRoute < 98);
				this.route3.getListHistoricalPeople().add(person);
			} else {
				int montecarloCompleteRoute = RandomUtils.randomBetween(1, 100);
				int gender = RandomUtils.randomBetween(1, 100);
				char charGender = ' ';
				if (gender < 80) {
					charGender = 'H';
				} else {
					charGender = 'M';
				}
				Person person = new Person((i + 1), charGender, "10:" + auxMinutes);
				person.setDuration(RandomUtils.randomBetween(route4.getTimeMin(), route4.getTimeMax()));
				person.setFinalTime(person.getFinalTime(person.getInitialTime(), person.getDuration()));
				person.setCompleted(montecarloCompleteRoute < 71);
				this.route4.getListHistoricalPeople().add(person);
			}
		}

	}
	
	/**
	 * Método usado para inicializar las rutas.
	 */
	private void initializeRoutes() {
		this.routeUtils = new RouteUtils();
		this.route1 = routeUtils.getRoute1();
		this.route1.setTimeMin(3);
		this.route1.setTimeMax(6);

		this.route2 = routeUtils.getRoute2();
		this.route2.setTimeMin(2);
		this.route2.setTimeMax(4);

		this.route3 = routeUtils.getRoute3();
		this.route3.setTimeMin(3);
		this.route3.setTimeMax(5);

		this.route4 = routeUtils.getRoute4();
		this.route4.setTimeMin(2);
		this.route4.setTimeMax(7);
	}

	
	/**
	 * Metodo usado para imprimir por consola, las estadisticas y las personas
	 * del escenario 3 a manera de prueba.
	 */
	public void showStatistics() {
		System.out.println("Estadisticas ruta 1");
		for (int i = 0; i < route1.getListHistoricalPeople().size(); i++) {
			Person person = route1.getListHistoricalPeople().get(i);
			System.out.println("Persona " + person.getNumberOfPerson() + " - Hora inicio: " + person.getInitialTime()
					+ " - Hora Fin: " + person.getFinalTime() + " - Duracion: " + person.getDuration()
					+ " - Ruta Completada: " + person.isCompleted());
		}
		
		System.out.println("Estadisticas ruta 2");
		for (int i = 0; i < route2.getListHistoricalPeople().size(); i++) {
			Person person = route2.getListHistoricalPeople().get(i);
			System.out.println("Persona " + person.getNumberOfPerson() + " - Hora inicio: " + person.getInitialTime()
					+ " - Hora Fin: " + person.getFinalTime() + " - Duracion: " + person.getDuration()
					+ " - Ruta Completada: " + person.isCompleted());
		}
		System.out.println("Estadisticas ruta 3");
		for (int i = 0; i < route3.getListHistoricalPeople().size(); i++) {
			Person person = route3.getListHistoricalPeople().get(i);
			System.out.println("Persona " + person.getNumberOfPerson() + " - Hora inicio: " + person.getInitialTime()
			+ " - Hora Fin: " + person.getFinalTime() + " - Duracion: " + person.getDuration()
			+ " - Ruta Completada: " + person.isCompleted());
		}
		System.out.println("Estadisticas ruta 4");
		for (int i = 0; i < route4.getListHistoricalPeople().size(); i++) {
			Person person = route4.getListHistoricalPeople().get(i);
			System.out.println("Persona " + person.getNumberOfPerson() + " - Hora inicio: " + person.getInitialTime()
			+ " - Hora Fin: " + person.getFinalTime() + " - Duracion: " + person.getDuration()
			+ " - Ruta Completada: " + person.isCompleted());
		}
	}

	@Override
	public double getAverageRoute1() {
		ArrayList<Person> personRoute1 = route1.getListHistoricalPeople();
		double durationAllPerson = 0;
		for (Person person : personRoute1) {
			durationAllPerson += person.getDuration();
		}
		return durationAllPerson / personRoute1.size();
	}

	@Override
	public double getAverageRoute2() {
		ArrayList<Person> personRoute2 = route2.getListHistoricalPeople();
		double durationAllPerson = 0;
		for (Person person : personRoute2) {
			durationAllPerson += person.getDuration();
		}
		return durationAllPerson / personRoute2.size();
	}

	@Override
	public double getAverageRoute3() {
		ArrayList<Person> personRoute3 = route3.getListHistoricalPeople();
		double durationAllPerson = 0;
		for (Person person : personRoute3) {
			durationAllPerson += person.getDuration();
		}
		return durationAllPerson / personRoute3.size();
	}
	
	@Override
	public double getAverageRoute4() {
		ArrayList<Person> personRoute4 = route4.getListHistoricalPeople();
		double durationAllPerson = 0;
		for (Person person : personRoute4) {
			durationAllPerson += person.getDuration();
		}
		return durationAllPerson / personRoute4.size();
	}
	

	@Override
	public ArrayList<Person> getPersonsOfRouteOne() {
		return route1.getListHistoricalPeople();
	}

	@Override
	public ArrayList<Person> getPersonsOfRouteTwo() {
		return route2.getListHistoricalPeople();
	}

	@Override
	public ArrayList<Person> getPersonsOfRouteThree() {
		return route3.getListHistoricalPeople();
	}

	@Override
	public ArrayList<Person> getPersonsOfRouteFour() {
		return route4.getListHistoricalPeople();
	}

	@Override
	public ArrayList<Person> getAllPersonsScenaryThree() {
		ArrayList<Person> auxListOfPersons = new ArrayList<Person>();
		for (Person person : route1.getListHistoricalPeople()) {
			auxListOfPersons.add(person);
		}
		for (Person person : route2.getListHistoricalPeople()) {
			auxListOfPersons.add(person);		
		}
		for (Person person : route3.getListHistoricalPeople()) {
			auxListOfPersons.add(person);
		}
		for (Person person : route4.getListHistoricalPeople()) {
			auxListOfPersons.add(person);
		}
		return auxListOfPersons;
	}

	@Override
	public int getAmountOfMondays() {
		return (quantityPersons / RandomUtils.randomBetween(34, 97)) + 1;
	}
}
