package logic.scenaryTwo;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import org.apache.commons.math3.distribution.NormalDistribution;
import utils.DatosGenerales;
import utils.ManagerDatos;
import utils.PlantillaDato;

/**
 * Clase que realiza la logica para dar tratamiento a los datos semilla y simular cada una de las rutas
 */
public class ScenaryTwo extends ManagerDatos implements InterfaceScenaryTwo {
    /**
     * Resultado de los datos simulados del tramo 1
     */
    private PlantillaDato[] datosTramo1;
    /**
     * Resultado de los datos simulados del tramo 2
     */
    private PlantillaDato[] datosTramo2;
    /**
     * Resultado de los datos simulados del tramo 3
     */
    private PlantillaDato[] datosTramo3;
    /**
     * Resultado de los datos simulados del tramo 4
     */
    private PlantillaDato[] datosTramo4;
    /**
     * Resultado de los datos simulados, pero en general, es decir los grupos o personas que 
     * terminaron la ruta completa de los 4 tramos y sus tiempos respectivos en toda la ruta
     */
    private PlantillaDato[] datosGenerales;
    /**
     * Representa la cantidad total de datos a simular
     */
    private int cantidadDeDatos;
    /**
     * Clase con la cual se generan datos pseudoaleatorios con distribucion normal
     */
    private NormalDistribution normalDistribution;
    /**
     * cantidad de segundos que tiene un minuto
     */
    private static final int SEGUNDOS_EN_UN_MINUTO = 60;
    /**
     * numero de minutos que tardo el experimento, 30 minutos de 10:30 am a 11:00 am
     */
    private static final int MINUTOS_EXPERIMENTO = 30;
    /**
     * Media del Tiempo que tardo el experimento (900 Segundos)
     */
    private static final int PROMEDIO_TIEMPO_COMPLETO_CAMINATA = SEGUNDOS_EN_UN_MINUTO * (MINUTOS_EXPERIMENTO / 2);

    /**
     * Se inicializa la clase para hacer las pruebas sobre los datos
     * Se simulan todas las rutas para obtener los datos resultado de acuerdo a las semillas
     * @param cantidadDeDatos numero de datos a simular en 1 Lunes
     */
    public ScenaryTwo(int cantidadDeDatos) {
        this.cantidadDeDatos = cantidadDeDatos;
        this.generarDatos();
    }

    /**
     * Genera los datos para los diferentes tramos, asi como los datos generales
     */
    private void generarDatos() {
        this.datosTramo1 = generarDatosTramo(tramo1, datosGeneralesTramo1, cantidadDeDatos);
        this.datosTramo2 = generarDatosSiguienteTramo(tramo2, datosGeneralesTramo2, obtenerSiCompletaron(datosTramo1));
        this.datosTramo3 = generarDatosSiguienteTramo(tramo2, datosGeneralesTramo3, obtenerSiCompletaron(datosTramo2));
        this.datosTramo4 = generarDatosSiguienteTramo(tramo4, datosGeneralesTramo4, obtenerSiCompletaron(datosTramo3));
        if (this.datosTramo4.length > 0) {
        	this.datosGenerales = generarDatosGenerales();
		}
//        System.out.println("\n\n\n------------    DatosGenerales    ---------------");
//        for (PlantillaDato plantillaDato : datosGenerales) {
//			System.out.println(plantillaDato);
//		}
    }

    /**
     * Determina el tiempo total de recorrido de aquellos que completaron la ruta completa
     * @return los individuos o grupos que completaron la ruta completa
     */
    private PlantillaDato[] generarDatosGenerales() {
    	ArrayList<PlantillaDato> datosResultado = new ArrayList<>();
    	int count = 0;
    	for (PlantillaDato plantillaDato : datosTramo4) {
    		if (plantillaDato.isCompletoLaRuta()) {
    			PlantillaDato plantillaDatoNew = null;
    			try {
					plantillaDatoNew = plantillaDato.clone();
				} catch (CloneNotSupportedException e) {
					e.printStackTrace();
				}
    			count++;
    			this.modificarTiempos(plantillaDatoNew);
    			plantillaDatoNew.setNumero(count);
    			datosResultado.add(plantillaDatoNew);
    		}
    	}
		return getArrayToVectorPlantillaDato(datosResultado);
	}

    /**
     * Dato de la ruta 4 al cual se le modificara el tiempo segun los resultados de las rutas anteriores
     * @param plantillaDato dato a modificar el tiempo
     */
	private void modificarTiempos(PlantillaDato plantillaDato) {
		PlantillaDato dato3 = this.getDatoTramoAnterior(datosTramo3, plantillaDato);
		PlantillaDato dato2 = this.getDatoTramoAnterior(datosTramo2, dato3);
		PlantillaDato dato1 = this.getDatoTramoAnterior(datosTramo1, dato2);
		plantillaDato.setTiempoCaminandoSegundos(dato1.getTiempoCaminandoSegundos());
		plantillaDato.setHoraInicio(dato1.getHoraInicio());
		plantillaDato.setTiempoCaminandoSegundos(plantillaDato.getTiempoCaminandoSegundos() + dato3.getTiempoCaminandoSegundos()
													+ dato2.getTiempoCaminandoSegundos() + dato1.getTiempoCaminandoSegundos());
	}

	/**
	 * Obtiene el dato de la ruta anterior al que corresponde un dato de una ruta posterior
	 * @param datosTramo todos los datos de un tramo anterior
	 * @param plantillaDato dato base de la ruta posterior
	 * @return El dato de la ruta anterior que corresponde al mismo en la ruta siguiente
	 */
	private PlantillaDato getDatoTramoAnterior(PlantillaDato[] datosTramo, PlantillaDato plantillaDato) {
		for (PlantillaDato dato : datosTramo) {
			if ((int) dato.getSegundosFin() == (int) plantillaDato.getSegundosInicio() && dato.isCompletoLaRuta()) {
				return dato;
			}
		}
		return null;
	}

	/**
     * Realiza el proceso de simulacion de los datos semilla para el primer (1 er) tramo unicamente
     * @param tramo datos semilla del tramo 1
     * @param datosGenerales representa las probabildiades de ser grupo o de completar la ruta de un tramo
     * @param catidad de elementos a simular
     * @return los datos generados luego de la simulacion
     */
    private PlantillaDato[] generarDatosTramo(ArrayList<PlantillaDato> tramo, DatosGenerales datosGenerales, int catidad) {
        PlantillaDato[] resultado = new PlantillaDato[catidad];
        int menorTiempoCaminata = this.getTiempoMenor(tramo);
        int mayorTiempoCaminata = this.getTiempoMayor(tramo);
        this.normalDistribution = new NormalDistribution(this.getPromerdioTiempo(tramo), this.getDesviacionEstandarTiempo(tramo));
        double[] tiemposCaminata = generarTiempos(menorTiempoCaminata, mayorTiempoCaminata, catidad);
        this.normalDistribution = new NormalDistribution(PROMEDIO_TIEMPO_COMPLETO_CAMINATA, PROMEDIO_TIEMPO_COMPLETO_CAMINATA);
        double[] tiemposInicioRuta = generarTiempos(0, SEGUNDOS_EN_UN_MINUTO * MINUTOS_EXPERIMENTO - 700, catidad);
        Arrays.sort(tiemposInicioRuta);
        this.normalDistribution = new NormalDistribution(getPromedioCantidadMujeres(tramo), getDesviacionCantidadMujeres(tramo));
        double[] cantidadMujeresGrupo = generarCantidades(getCantidadMenorMujeres(tramo), getCantidadMayorMujeres(tramo), catidad);
        this.normalDistribution = new NormalDistribution(getPromedioCantidadHombres(tramo), getDesviacionCantidadHombres(tramo));
        double[] cantidadHombresGrupo = generarCantidades(getCantidadMenorHombres(tramo), getCantidadMayorHombres(tramo), catidad);
        int cantidadHombres = 0;
        int cantidadMujeres = 0;
        for (int i = 0; i < resultado.length; i++) {
            String horaInicio = getFormatoDeHora((int) tiemposInicioRuta[i]);
            double tiempoFinalRuta = tiemposInicioRuta[i] + tiemposCaminata[i];
            String horaFin = getFormatoDeHora((int) tiempoFinalRuta);
            if (Math.random() <= datosGenerales.getProbabilidadDeSerGrupo() / 100) {
                double genero = Math.random();
                if (genero <= 0.5) {
                    cantidadMujeres = 1;
                } else {
                    cantidadHombres = 1;
                }
            } else {
                cantidadMujeres = (int) cantidadMujeresGrupo[i];
                cantidadHombres = (int) cantidadHombresGrupo[i];
                if (cantidadMujeres == 0 && cantidadHombres == 0) {
                    double genero = Math.random();
                    if (genero <= 0.5) {
                        cantidadMujeres = 1;
                    } else {
                        cantidadHombres = 1;
                    }
                }
            }
            boolean completoLaRuta = (Math.random() <= datosGenerales.getProbabilidadCompleto() / 100) ? true : false;
            resultado[i] = new PlantillaDato(i+1, (int) tiemposCaminata[i], cantidadHombres, cantidadMujeres,
                    completoLaRuta, tiemposInicioRuta[i], tiempoFinalRuta, horaInicio, horaFin);
//            System.out.println(tiemposCaminata[i] + " " + cantidadHombres + " " + cantidadMujeres + " " + completoLaRuta + " " + horaInicio + " " + horaFin);
        }
        return resultado;
    }
    
    /**
     * Realiza el proceso de simulacion de los datos semilla para los tramos posteriores al primero (2, 3 y 4)
     * @param tramo datos semilla del los tramos
     * @param datosGenerales representa las probabildiades de ser grupo o de completar la ruta de un tramo
     * @param tiemposFinales Representa los tiempos fin en segundos, de los datos de la ruta anterior
     * @return los datos generados luego de la simulacion
     */
    private PlantillaDato[] generarDatosSiguienteTramo(ArrayList<PlantillaDato> tramo, DatosGenerales datosGenerales, ArrayList<Double> tiemposFinales) {
//        System.out.println("-----------------------------");
        PlantillaDato[] resultado = new PlantillaDato[tiemposFinales.size()];
        int menorTiempoCaminata = this.getTiempoMenor(tramo);
        int mayorTiempoCaminata = this.getTiempoMayor(tramo);
        this.normalDistribution = new NormalDistribution(this.getPromerdioTiempo(tramo), this.getDesviacionEstandarTiempo(tramo));
        double[] tiemposCaminata = generarTiempos(menorTiempoCaminata, mayorTiempoCaminata, tiemposFinales.size());
        this.normalDistribution = new NormalDistribution(PROMEDIO_TIEMPO_COMPLETO_CAMINATA, PROMEDIO_TIEMPO_COMPLETO_CAMINATA);
        this.normalDistribution = new NormalDistribution(getPromedioCantidadMujeres(tramo), getDesviacionCantidadMujeres(tramo));
        double[] cantidadMujeresGrupo = generarCantidades(getCantidadMenorMujeres(tramo), getCantidadMayorMujeres(tramo), tiemposFinales.size());
        this.normalDistribution = new NormalDistribution(getPromedioCantidadHombres(tramo), getDesviacionCantidadHombres(tramo));
        double[] cantidadHombresGrupo = generarCantidades(getCantidadMenorHombres(tramo), getCantidadMayorHombres(tramo), tiemposFinales.size());
        int cantidadHombres = 0;
        int cantidadMujeres = 0;
        Collections.sort(tiemposFinales);
        for (int i = 0; i < resultado.length; i++) {
            double finales = tiemposFinales.get(i);
            String horaInicio = getFormatoDeHora((int) finales);
            double tiempoFinalRuta = tiemposFinales.get(i) + tiemposCaminata[i];
            String horaFin = getFormatoDeHora((int) tiempoFinalRuta);
            if (Math.random() <= datosGenerales.getProbabilidadDeSerGrupo() / 100) {
                double genero = Math.random();
                if (genero <= 0.5) {
                    cantidadMujeres = 1;
                } else {
                    cantidadHombres = 1;
                }
            } else {
                cantidadMujeres = (int) cantidadMujeresGrupo[i];
                cantidadHombres = (int) cantidadHombresGrupo[i];
                if (cantidadMujeres == 0 && cantidadHombres == 0) {
                    double genero = Math.random();
                    if (genero <= 0.5) {
                        cantidadMujeres = 1;
                    } else {
                        cantidadHombres = 1;
                    }
                }
            }
            boolean completoLaRuta = (Math.random() <= datosGenerales.getProbabilidadCompleto() / 100) ? true : false;
            resultado[i] = new PlantillaDato(i+1, (int) tiemposCaminata[i], cantidadHombres,
                    cantidadMujeres, completoLaRuta, tiemposFinales.get(i), tiempoFinalRuta, horaInicio, horaFin);
//            System.out.println(tiemposCaminata[i] + " " + cantidadHombres + " " + cantidadMujeres + " " + completoLaRuta + " " + horaInicio + " " + horaFin);
        }
        return resultado;
    }

    /**
     * Metodo que obtiene los tiempos finales de las personas o grupos que completaron un tramo
     * @param datosTramo cualquier conjunto de datos de un tramo, tanto semilla como simulados
     * @return los tiempos finales (en segundos) de las personas o grupos que completaron un tramo
     */
    public ArrayList<Double> obtenerSiCompletaron(PlantillaDato[] datosTramo) {
        ArrayList<Double> horasFinales = new ArrayList<>();
        for (PlantillaDato plantillaDato : datosTramo) {
            if (plantillaDato.isCompletoLaRuta()) {
                double horaFinal = (Double.parseDouble(plantillaDato.getHoraFin().substring(0, 2)) * 60)
                        + Double.parseDouble(plantillaDato.getHoraFin().substring(3, 5));
                horasFinales.add(horaFinal);
            }
        }
        return horasFinales;
    }
    
    /**
     * Metodo que obtiene la cantidad de personas o grupos que no completaron un tramo
     * @param datosTramo cualquier conjunto de datos de un tramo, tanto semilla como simulados
     * @return el numero de personas o grupos que completaron la ruta
     */
    public int obtenerNoCompletaron(PlantillaDato[] datosTramo) {
    	int cantidad = 0;
    	for (PlantillaDato plantillaDato : datosTramo) {
    		if (!plantillaDato.isCompletoLaRuta()) {
    			cantidad++;
    		}
    	}
    	return cantidad;
    }

    /**
     * Retorna el formato de fecha en forma de String, a partir de la cantidad de segundos que 
     * tarda una persona o grupo en completar su recorrido 
     * @param d representa el tiempo en segundos que tarda caminando una persona o grupo
     * @return el formato de fecha en forma de String
     */
    private String getFormatoDeHora(int d) {
        String segundos = Integer.toString(d % 60);
        String minutos = Integer.toString(d / 60);
        if (Integer.parseInt(minutos) < 10 && Integer.parseInt(segundos) < 10) {
            return "0" + minutos + ":" + "0" + segundos;
        } else if (Integer.parseInt(minutos) < 10) {
            return "0" + minutos + ":" + segundos;
        } else if (Integer.parseInt(segundos) < 10) {
            return minutos + ":" + "0" + segundos;
        }
        return minutos + ":" + segundos;
    }

    /**
     * Metodo que genera datos de tiempos de caminata, de forma pseudoaleatoria por
     * medio de una distribucion normal
     * @param menor dato menor a partir del cual generar los numeros
     * @param mayor dato mayor hasta el cual generar los numeros
     * @param catidad de datos a generar
     * @return la lista de numeros pseudoaleatorios generados
     */
    private double[] generarTiempos(double menor, double mayor, int catidad) {
        double[] resultado = new double[catidad];
        for (int i = 0; i < catidad; i++) {
            double numero = 0;
            while ((numero = this.normalDistribution.sample()) < menor || numero > mayor);
            resultado[i] = numero;
        }
		return resultado;
    }

    /**
     * Genera de manera pseudoaleatoria por medio de distribucion normal, la 
     * cantidad de hombres y mujeres que cruzaron un tramo
     * @param menor dato menor a partir del cual generar los numeros
     * @param mayor dato mayor hasta el cual generar los numeros
     * @param catidad de datos a generar
     * @return la lista de numeros pseudoaleatorios generados
     */
    private double[] generarCantidades(double menor, double mayor, int catidad) {
        double[] resultado = new double[catidad];
        for (int i = 0; i < catidad; i++) {
            double numero = 0;
            while ((numero = this.normalDistribution.sample()) < menor || numero > mayor + 1);
            resultado[i] = numero;
        }
        return resultado;
    }

    
    
    // +++++++++    Metodos datos resultado de la simulacion por Tramo y en General    +++++++++++++
    
    @Override
    public PlantillaDato[] getDatosTramo1() {
        return datosTramo1;
    }
    
    @Override
    public PlantillaDato[] getDatosTramo2() {
        return datosTramo2;
    }

    @Override
    public PlantillaDato[] getDatosTramo3() {
        return datosTramo3;
    }

    @Override
    public PlantillaDato[] getDatosTramo4() {
        return datosTramo4;
    }

    @Override
    public PlantillaDato[] getDatosCompletaronRecorrido() {
        return datosGenerales;
    }
    
    
    
    
    // ------------------------- Metodos de Datos Generales de los Tramos ------------------------------
    
    
    // +++++++++    Metodos de cantidad de personas que SI completaron cada Tramo    +++++++++++++
    
    @Override
    public int getCantidadCompletaronTramo1() {
    	return this.obtenerSiCompletaron(datosTramo1).size();
	}
    
    @Override
    public int getCantidadCompletaronTramo2() {
    	return this.obtenerSiCompletaron(datosTramo2).size();
    }
    
    @Override
    public int getCantidadCompletaronTramo3() {
    	return this.obtenerSiCompletaron(datosTramo3).size();
    }
    
    @Override
    public int getCantidadCompletaronTramo4() {
    	return this.obtenerSiCompletaron(datosTramo4).size();
    }
    
    @Override
    public int getCantidadCompletaronTodoElRecorrido() {
    	return this.obtenerSiCompletaron(datosGenerales).size();
    }
    
    
    
    // +++++++++    Metodos de cantidad de personas que NO completaron cada Tramo    +++++++++++++
    
    @Override
    public int getCantidadNoCompletaronTramo1() {
    	return this.obtenerNoCompletaron(datosTramo1);
	}
    
    @Override
	public int getCantidadNoCompletaronTramo2() {
    	return this.obtenerNoCompletaron(datosTramo2);
    }
    
    @Override
    public int getCantidadNoCompletaronTramo3() {
    	return this.obtenerNoCompletaron(datosTramo3);
    }
    
    @Override
    public int getCantidadNoCompletaronTramo4() {
    	return this.obtenerNoCompletaron(datosTramo4);
    }
    
    
    
    
    // +++++++++    Metodos de promedio de tiempo caminando durante cada Tramo    +++++++++++++
    
    @Override
    public double getPromedioTiempoCaminandoTramo1() {
    	return this.getPromerdioTiempo(this.getVectorToArrayPlantillaDato(datosTramo1));
    }
    
    @Override
    public double getPromedioTiempoCaminandoTramo2() {
    	return this.getPromerdioTiempo(this.getVectorToArrayPlantillaDato(datosTramo2));
    }
    
    @Override
    public double getPromedioTiempoCaminandoTramo3() {
    	return this.getPromerdioTiempo(this.getVectorToArrayPlantillaDato(datosTramo3));
    }
    
    @Override
    public double getPromedioTiempoCaminandoTramo4() {
    	return this.getPromerdioTiempo(this.getVectorToArrayPlantillaDato(datosTramo4));
    }
    
    @Override
    public double getPromedioTiempoCaminandoTodoElRecorrido() {
    	return this.getPromerdioTiempo(this.getVectorToArrayPlantillaDato(datosGenerales));
    }
    
    /**
     * Transpasa los datos de un vector de la clase PlantillaDato a ArrayList
     * @param datos cualquier conjunto de datos de un tramo
     * @return los datos de un tramo en forma de ArrayList
     */
    private ArrayList<PlantillaDato> getVectorToArrayPlantillaDato(PlantillaDato[] datos) {
    	ArrayList<PlantillaDato> resultado = new ArrayList<>();
    	for (PlantillaDato plantillaDato : datos) {
			resultado.add(plantillaDato);
		}
    	return resultado;
	}
    
	private PlantillaDato[] getArrayToVectorPlantillaDato(ArrayList<PlantillaDato> datos) {
		PlantillaDato[] datosResultado = new PlantillaDato[datos.size()];
		for (int i = 0; i < datosResultado.length; i++) {
			datosResultado[i] = datos.get(i);
		}
		return datosResultado;
	}
}
