package logic.scenaryTwo;

import utils.PlantillaDato;

public interface InterfaceScenaryTwo {
	
    // +++++++++    Metodos datos resultado de la simulacion por Tramo y en General    +++++++++++++
	
	/**
     * @return los datos resultado de la simulacion, para el tramo 1
     */
    public PlantillaDato[] getDatosTramo1();
    
    /**
     * @return los datos resultado de la simulacion, para el tramo 2
     */
    public PlantillaDato[] getDatosTramo2();

    /**
     * @return los datos resultado de la simulacion, para el tramo 3
     */
    public PlantillaDato[] getDatosTramo3();

    /**
     * @return los datos resultado de la simulacion, para el tramo 4
     */
    public PlantillaDato[] getDatosTramo4();

    /**
     * @return los datos resultado de la simulacion, de aquellos grupos o personas que completaron el recorrido completo
     */
    public PlantillaDato[] getDatosCompletaronRecorrido();

    

    
    // ------------------------- Metodos de Datos Generales de los Tramos ------------------------------
    
	// +++++++++    Metodos de cantidad de personas que SI completaron cada Tramo y en General    +++++++++++++
    
	/**
	 * Cantidad de personas que completaron la ruta 1
	 */
    public int getCantidadCompletaronTramo1();
    /**
	 * Cantidad de personas que completaron la ruta 2
	 */
    public int getCantidadCompletaronTramo2();
    /**
	 * Cantidad de personas que completaron la ruta 3
	 */
    public int getCantidadCompletaronTramo3();
    /**
	 * Cantidad de personas que completaron la ruta 4
	 */
    public int getCantidadCompletaronTramo4();
    /**
	 * Cantidad de personas que completaron Todo el Recorrido Por las personas que completaron toda la ruta
	 */
    public int getCantidadCompletaronTodoElRecorrido();

    
    
    
    // +++++++++    Metodos de cantidad de personas que NO completaron cada Tramo    +++++++++++++

    /**
	 * Cantidad de personas que NO completaron la ruta 1
	 */
    public int getCantidadNoCompletaronTramo1();
    /**
	 * Cantidad de personas que NO completaron la ruta 2
	 */
	public int getCantidadNoCompletaronTramo2();
	/**
	 * Cantidad de personas que NO completaron la ruta 3
	 */
    public int getCantidadNoCompletaronTramo3();
    /**
	 * Cantidad de personas que NO completaron la ruta 4
	 */
    public int getCantidadNoCompletaronTramo4();
    
    
    
    
    // +++++++++    Metodos de promedio de tiempo en segundos caminando durante cada Tramo y en General    +++++++++++++
    
    /**
	 * Tiempo promedio en segundos caminando en la ruta 1
	 */
    public double getPromedioTiempoCaminandoTramo1();
    /**
	 * Tiempo promedio en segundos caminando en la ruta 2
	 */
    public double getPromedioTiempoCaminandoTramo2();
    /**
	 * Tiempo promedio en segundos caminando en la ruta 3
	 */
    public double getPromedioTiempoCaminandoTramo3();
    /**
	 * Tiempo promedio en segundos caminando en la ruta 4
	 */
    public double getPromedioTiempoCaminandoTramo4();
    /**
	 * Tiempo promedio en segundos caminando en Todo el Recorrido Por las personas que completaron toda la ruta
	 */
    public double getPromedioTiempoCaminandoTodoElRecorrido();
}
